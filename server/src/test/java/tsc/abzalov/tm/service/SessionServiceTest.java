package tsc.abzalov.tm.service;

import lombok.val;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.jupiter.api.*;
import tsc.abzalov.tm.api.repository.ISessionRepository;
import tsc.abzalov.tm.api.repository.IUserRepository;
import tsc.abzalov.tm.api.service.IAuthService;
import tsc.abzalov.tm.api.service.ISessionService;
import tsc.abzalov.tm.api.service.IUserService;
import tsc.abzalov.tm.api.service.property.IApplicationPropertyService;
import tsc.abzalov.tm.exception.auth.AccessDeniedException;
import tsc.abzalov.tm.exception.auth.IncorrectCredentialsException;
import tsc.abzalov.tm.exception.auth.SessionIsInactiveException;
import tsc.abzalov.tm.exception.auth.UserLockedException;
import tsc.abzalov.tm.exception.data.EntityNotFoundException;
import tsc.abzalov.tm.model.Session;
import tsc.abzalov.tm.model.User;
import tsc.abzalov.tm.repository.SessionRepository;
import tsc.abzalov.tm.repository.UserRepository;

import java.util.Date;

import static org.junit.jupiter.api.Assertions.*;

class SessionServiceTest {

//    @NotNull
//    private static final String LOGIN = "Login";
//
//    @NotNull
//    private static final String PASSWORD = "Password";
//
//    @NotNull
//    private static final String FIRSTNAME = "Firstname";
//
//    @NotNull
//    private static final String LASTNAME = "Lastname";
//
//    @NotNull
//    private static final String EMAIL = "Email";
//
//    @Nullable
//    private IUserRepository userRepository;
//
//    @NotNull
//    private final IApplicationPropertyService propertyService = new ApplicationPropertyService();
//
//    @NotNull
//    private final IUserService userService = new UserService(propertyService);
//
//    @NotNull
//    private final ISessionRepository sessionRepository = new SessionRepository();
//
//    @NotNull
//    private final ISessionService sessionService = new SessionService(sessionRepository, userService, propertyService);
//
//    @NotNull
//    private final IAuthService authService = new AuthService(userService, propertyService);
//
//    @NotNull
//    private final User user = new User();
//
//    @NotNull
//    private final String userId = this.user.getId();
//
//    {
//        this.user.setLogin(LOGIN);
//        this.user.setHashedPassword(PASSWORD);
//        this.user.setEmail(EMAIL);
//    }
//
//    @BeforeEach
//    void setUp() {
//        authService.register(LOGIN, PASSWORD, FIRSTNAME, LASTNAME, EMAIL);
//    }
//
//    @AfterEach
//    void tearDown() {
//        userRepository.clear();
//    }
//
//    @Test
//    @Tag("Unit")
//    @DisplayName("Open Session Test")
//    void openSession() {
//        assertAll(
//                () -> assertThrows(
//                        IncorrectCredentialsException.class,
//                        () -> sessionService.openSession(null, PASSWORD)
//                ),
//                () -> assertThrows(
//                        IncorrectCredentialsException.class,
//                        () -> sessionService.openSession(LOGIN, null)
//                ),
//                () -> assertThrows(
//                        EntityNotFoundException.class,
//                        () -> sessionService.openSession(LOGIN + 1, PASSWORD)
//                ),
//                () -> assertThrows(
//                        UserLockedException.class,
//                        () -> {
//                            userRepository.lockUnlockByLogin(LOGIN);
//                            sessionService.openSession(LOGIN, PASSWORD);
//                            userRepository.lockUnlockByLogin(LOGIN);
//                        }
//                ),
//                () -> assertThrows(
//                        IncorrectCredentialsException.class,
//                        () -> {
//                            @NotNull val newUser = new User();
//                            newUser.setLogin(LOGIN + LOGIN);
//                            userRepository.create(newUser);
//                            sessionService.openSession(LOGIN + LOGIN, PASSWORD + PASSWORD);
//                        }
//                ),
//                () -> assertThrows(
//                        AccessDeniedException.class,
//                        () -> {
//                            userRepository.lockUnlockByLogin(LOGIN);
//                            sessionService.openSession(LOGIN, PASSWORD + 1);
//                        }
//                ),
//                () -> assertNotNull(sessionService.openSession(LOGIN, PASSWORD))
//        );
//    }
//
//    @Test
//    @Tag("Unit")
//    @DisplayName("Close Session Test")
//    void closeSession() {
//        assertAll(
//                () -> assertThrows(
//                        SessionIsInactiveException.class,
//                        () -> sessionService.closeSession(null)
//                ),
//                () -> {
//                    @NotNull val session = sessionService.openSession(LOGIN, PASSWORD);
//                    sessionService.closeSession(session);
//                    assertNull(sessionRepository.findSession(session));
//                }
//        );
//    }
//
//    @Test
//    @Tag("Unit")
//    @DisplayName("Validate Test")
//    void validate() {
//        assertAll(
//                () -> assertThrows(
//                        AccessDeniedException.class,
//                        () -> sessionService.validate(null)
//                ),
//                () -> assertThrows(
//                        AccessDeniedException.class,
//                        () -> {
//                            @NotNull val session = new Session();
//                            sessionService.validate(session);
//                        }
//                ),
//                () -> assertThrows(
//                        AccessDeniedException.class,
//                        () -> {
//                            @NotNull val session = new Session();
//                            session.setUserId(this.userId);
//                            sessionService.validate(session);
//                        }
//                ),
//                () -> assertThrows(
//                        AccessDeniedException.class,
//                        () -> {
//                            @NotNull val session = new Session();
//                            session.setUserId(this.userId + 1);
//                            session.setTimestamp(new Date().getTime());
//                            sessionService.validate(session);
//                        }
//                ),
//                () -> assertThrows(
//                        AccessDeniedException.class,
//                        () -> {
//                            @NotNull val session = new Session();
//                            session.setUserId(this.userId);
//                            session.setTimestamp(new Date().getTime());
//                            sessionService.validate(session);
//                        }
//                )
//        );
//    }
//
//    @Test
//    @Tag("Unit")
//    @DisplayName("Validate Admin Permissions Test")
//    void validateAdminPermissions() {
//        assertThrows(
//                AccessDeniedException.class,
//                () -> {
//                    @NotNull val session = sessionService.openSession(LOGIN, PASSWORD);
//                    sessionService.validateAdminPermissions(session, userService);
//                }
//        );
//    }
//
//    @Test
//    @Tag("Unit")
//    @DisplayName("Find Session Test")
//    void findSession() {
//        assertAll(
//                () -> assertThrows(
//                        AccessDeniedException.class,
//                        () -> sessionService.findSession(null)
//                ),
//                () -> assertThrows(
//                        AccessDeniedException.class,
//                        () -> {
//                            @NotNull val session = new Session();
//                            sessionService.findSession(session);
//                        }
//                ),
//                () -> assertThrows(
//                        AccessDeniedException.class,
//                        () -> {
//                            @NotNull val session = new Session();
//                            session.setUserId(this.userId);
//                            sessionService.findSession(session);
//                        }
//                ),
//                () -> assertThrows(
//                        AccessDeniedException.class,
//                        () -> {
//                            @NotNull val session = new Session();
//                            session.setUserId(this.userId + 1);
//                            session.setTimestamp(new Date().getTime());
//                            sessionService.findSession(session);
//                        }
//                ),
//                () -> assertThrows(
//                        AccessDeniedException.class,
//                        () -> {
//                            @NotNull val session = new Session();
//                            session.setUserId(this.userId);
//                            session.setTimestamp(new Date().getTime());
//                            sessionService.findSession(session);
//                        }
//                )
//        );
//    }

}