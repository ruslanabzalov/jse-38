package tsc.abzalov.tm.util;

import lombok.experimental.UtilityClass;
import org.jetbrains.annotations.NotNull;

@UtilityClass
public class LiteralConst {

    @NotNull
    public static final String DEFAULT_NAME = "Name is empty";

    @NotNull
    public static final String DEFAULT_DESCRIPTION = "Description is empty";

    @NotNull
    public static final String IS_NOT_STARTED = "Is not started";

    @NotNull
    public static final String IS_NOT_ENDED = "Is not ended";

    @NotNull
    public static final String DATE_TIME_FORMAT = "dd.MM.yyyy HH:mm:ss";

    @NotNull
    public static final String DEFAULT_LOGIN = "Login is empty";

    @NotNull
    public static final String DEFAULT_FIRSTNAME = "Firstname is empty";

    @NotNull
    public static final String DEFAULT_LASTNAME = "Lastname is empty";

    @NotNull
    public static final String DEFAULT_EMAIL = "Email is empty";

    @NotNull
    public static final String ACTIVE = "Active";

    @NotNull
    public static final String LOCKED = "Locked";

    @NotNull
    public static final String ENDPOINTS_PACKAGE = "tsc.abzalov.tm.endpoint";

    @NotNull
    public static final String DEFAULT_APP_VERSION = "0.0.0";

    @NotNull
    public static final String DEFAULT_DEVELOPER_NAME = "Unknown";

    @NotNull
    public static final String DEFAULT_DEVELOPER_EMAIL = "email@mail.com";

    @NotNull
    public static final String DEFAULT_PASSWORD_HASHING_SALT = "qwerty";

    @NotNull
    public static final String DEFAULT_PASSWORD_HASHING_COUNTER = "10";

    @NotNull
    public static final String DEFAULT_SESSION_HASHING_SALT = "poiuytr";

    @NotNull
    public static final String DEFAULT_SESSION_HASHING_COUNTER = "20";

    @NotNull
    public static final String DEFAULT_SERVER_HOST = "localhost";

    @NotNull
    public static final String DEFAULT_SERVER_PORT = "8080";

    @NotNull
    public static final String DEFAULT_DATABASE_URL = "jdbc:mysql://localhost:3306/task_manager";

    @NotNull
    public static final String DEFAULT_DATABASE_USER = "root";

    @NotNull
    public static final String DEFAULT_DATABASE_PASSWORD = "Liverpool12";

}
