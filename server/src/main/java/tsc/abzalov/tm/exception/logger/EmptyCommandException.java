package tsc.abzalov.tm.exception.logger;

import tsc.abzalov.tm.exception.AbstractException;

public final class EmptyCommandException extends AbstractException {

    public EmptyCommandException() {
        super("Command is empty!");
    }

}
