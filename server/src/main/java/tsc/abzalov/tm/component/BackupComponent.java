package tsc.abzalov.tm.component;

import org.jetbrains.annotations.NotNull;
import tsc.abzalov.tm.api.endpoint.IEndpointLocator;

import static java.util.concurrent.TimeUnit.SECONDS;

public final class BackupComponent extends AbstractBackgroundTaskComponent {

    public BackupComponent(@NotNull final IEndpointLocator endpointLocator) {
        super(endpointLocator);
    }

    @Override
    public void run() {
        loadBackup();
        scheduledBackupSave();
    }

    private void loadBackup() {
        executeBackupLoadCommand();
    }

    private void scheduledBackupSave() {
        getScheduledExecutorService()
                .scheduleWithFixedDelay(this::executeBackupSaveCommand, INITIAL_DELAY, BACKUP_DELAY, SECONDS);
    }

    private void executeBackupLoadCommand() {
        getEndpointLocator().getFileBackupService().autoLoadBackup();
    }

    private void executeBackupSaveCommand() {
        getEndpointLocator().getFileBackupService().autoSaveBackup();
    }

}
