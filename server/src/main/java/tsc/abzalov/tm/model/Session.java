package tsc.abzalov.tm.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

@Data
@EqualsAndHashCode(callSuper = true)
@JsonIgnoreProperties(ignoreUnknown = true)
public final class Session extends AbstractEntity implements Cloneable {

    @Nullable
    private Long userId;

    @Nullable
    private Long timestamp;

    @Nullable
    private String signature;

    @Nullable
    public Session clone() {
        try {
            return (Session) super.clone();
        } catch (@NotNull final CloneNotSupportedException exception) {
            return null;
        }
    }

}
