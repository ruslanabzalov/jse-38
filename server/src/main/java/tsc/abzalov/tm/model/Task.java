package tsc.abzalov.tm.model;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.val;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.io.Serializable;

@Data
@EqualsAndHashCode(callSuper = true)
public final class Task extends AbstractBusinessEntity implements Serializable, Cloneable {

    @Nullable
    private Long projectId;

    @Nullable
    @Override
    public Task clone() {
        try {
            return (Task) super.clone();
        }
        catch (@NotNull final CloneNotSupportedException exception) {
            return null;
        }
    }

    @NotNull
    @Override
    public String toString() {
        @NotNull val superStringInterpretation = super.toString();
        return superStringInterpretation.replace("]", "; Project ID: " + this.projectId + "]");
    }

}
