package tsc.abzalov.tm.api;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import tsc.abzalov.tm.model.AbstractBusinessEntity;

import java.util.List;


public interface IBusinessEntityService<T extends AbstractBusinessEntity> extends IService<T> {

    @Override
    long size();

    @Override
    boolean isEmpty();

    @Override
    void create(@Nullable T entity);

    @Override
    void addAll(@Nullable List<T> entities);

    @Override
    @NotNull List<T> findAll();

    @Override
    void clear();

    long size(@Nullable Long userId);

    boolean isEmpty(@Nullable Long userId);

    int indexOf(@Nullable Long userId, @Nullable T entity);

    @NotNull
    List<T> findAll(@Nullable Long userId);

    @Nullable
    T findById(@Nullable Long id);

    @Nullable
    T findByIndex(@Nullable Long userId, int index);

    @Nullable
    T findByName(@Nullable Long userId, @Nullable String name);

    @Nullable
    T editById(@Nullable Long id, @Nullable String name, @Nullable String description);

    @Nullable
    T editByName(@Nullable Long userId, @Nullable String name, @Nullable String description);

    void clear(@Nullable Long userId);

    void removeById(@Nullable Long id);

    void removeByName(@Nullable Long userId, @Nullable String name);

    @Nullable
    T startById(@Nullable Long id);

    @Nullable
    T endById(@Nullable Long id);

    @NotNull
    List<T> sortByName(@Nullable Long userId);

    @NotNull
    List<T> sortByStartDate(@Nullable Long userId);

    @NotNull
    List<T> sortByEndDate(@Nullable Long userId);

    @NotNull
    List<T> sortByStatus(@Nullable Long userId);

}
