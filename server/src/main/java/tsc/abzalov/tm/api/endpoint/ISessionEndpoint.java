package tsc.abzalov.tm.api.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import tsc.abzalov.tm.model.Session;

public interface ISessionEndpoint {

    @NotNull
    Session openSession(@Nullable String login, @Nullable String password);

    void closeSession(@Nullable Session session);

    @Nullable
    Session findSession(@Nullable Session session);

}
