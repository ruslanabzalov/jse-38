package tsc.abzalov.tm.api.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import tsc.abzalov.tm.model.Session;
import tsc.abzalov.tm.model.Task;

import java.util.List;

public interface ITaskEndpoint {

    long sizeTasks(@Nullable Session session);

    boolean isEmptyTaskList(@Nullable Session session);

    void createTask(@Nullable Session session, @NotNull String name, @NotNull String description);

    void addAllTasks(@Nullable Session session, @Nullable List<Task> tasks);

    @Nullable
    Task findTasksById(@Nullable Session session, @Nullable Long id);

    void clearAllTasks(@Nullable Session session);

    long tasksSize(@Nullable Session session);

    boolean areTasksEmpty(@Nullable Session session);

    int taskIndex(@Nullable Session session, @Nullable Task entity);

    @NotNull
    List<Task> findAllTasks(@Nullable Session session);

    @Nullable
    Task findTaskById(@Nullable Session session, @Nullable Long id);

    @Nullable
    Task findTaskByIndex(@Nullable Session session, int index);

    @Nullable
    Task findTaskByName(@Nullable Session session, @Nullable String name);

    @Nullable
    Task editTaskById(@Nullable Session session, @Nullable Long id,
                      @Nullable String name, @Nullable String description);

    @Nullable
    Task editTaskByName(@Nullable Session session, @Nullable String name, @Nullable String description);

    void clearTasks(@Nullable Session session);

    void removeTaskById(@Nullable Session session, @Nullable Long id);

    void removeTaskByName(@Nullable Session session, @Nullable String name);

    @Nullable
    Task startTaskById(@Nullable Session session, @Nullable Long id);

    @Nullable
    Task endTaskById(@Nullable Session session, @Nullable Long id);

    @NotNull
    List<Task> sortTasksByName(@Nullable Session session);

    @NotNull
    List<Task> sortTasksByStartDate(@Nullable Session session);

    @NotNull
    List<Task> sortTasksByEndDate(@Nullable Session session);

    @NotNull
    List<Task> sortTasksByStatus(@Nullable Session session);

}
