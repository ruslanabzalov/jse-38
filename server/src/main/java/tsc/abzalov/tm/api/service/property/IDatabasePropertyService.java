package tsc.abzalov.tm.api.service.property;

import org.jetbrains.annotations.NotNull;

public interface IDatabasePropertyService {

    @NotNull
    String getDatabaseUrl();

    @NotNull
    String getDatabaseUser();

    @NotNull
    String getDatabasePassword();

}
