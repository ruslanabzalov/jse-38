package tsc.abzalov.tm.bootstrap;

import lombok.Getter;
import lombok.SneakyThrows;
import lombok.val;
import lombok.var;
import org.jetbrains.annotations.NotNull;
import org.reflections.Reflections;
import tsc.abzalov.tm.api.endpoint.IEndpointLocator;
import tsc.abzalov.tm.api.service.*;
import tsc.abzalov.tm.api.service.property.IApplicationPropertyService;
import tsc.abzalov.tm.component.AbstractBackgroundTaskComponent;
import tsc.abzalov.tm.component.BackupComponent;
import tsc.abzalov.tm.endpoint.AbstractEndpoint;
import tsc.abzalov.tm.service.*;

import javax.xml.ws.Endpoint;
import java.io.File;
import java.io.IOException;
import java.lang.reflect.Modifier;
import java.nio.file.Files;
import java.nio.file.Paths;

import static tsc.abzalov.tm.enumeration.Role.ADMIN;
import static tsc.abzalov.tm.util.LiteralConst.ENDPOINTS_PACKAGE;
import static tsc.abzalov.tm.util.SystemUtil.getApplicationPid;

@Getter
public final class CommandBootstrap implements IServiceLocator, IEndpointLocator {

    @NotNull
    private final IApplicationPropertyService propertyService = new ApplicationPropertyService();

    @NotNull
    private final IConnectionService connectionService = new ConnectionService(propertyService);

    @NotNull
    private final IProjectService projectService = new ProjectService(connectionService);

    @NotNull
    private final ITaskService taskService = new TaskService(connectionService);

    @NotNull
    private final ProjectTaskService projectTaskService = new ProjectTaskService(connectionService);

    @NotNull
    private final IUserService userService = new UserService(connectionService, propertyService);

    @NotNull
    private final IAuthService authService = new AuthService(userService, propertyService);

    @NotNull
    private final ISessionService sessionService = new SessionService(connectionService, userService, propertyService);

    @NotNull
    private final IFileBackupService fileBackupService = new FileBackupService(this);

    @NotNull
    private final ILoggerService loggerService = new LoggerService();

    @NotNull
    private final AbstractBackgroundTaskComponent backupComponent = new BackupComponent(this);

    {
        runBackgroundTasks();
        propertyService.initLocalProperties();
        registerApplicationPid();
        registerEndpoints();

        userService.clear();
        projectService.clear();
        taskService.clear();

        @NotNull val adminLogin = "admin";
        @NotNull val adminPassword = "admin";
        @NotNull val adminRole = ADMIN;
        @NotNull val adminFirstname = "Admin";
        @NotNull val adminLastname = "";
        @NotNull val adminEmail = "admin@mail.com";

        userService.create(adminLogin, adminPassword, adminRole, adminFirstname, adminLastname, adminEmail);
    }

    private void runBackgroundTasks() {
        backupComponent.run();
    }

    private void registerEndpoints() {
        @NotNull val host = propertyService.getServerHost();
        @NotNull val port = propertyService.getServerPort();
        initEndpoints(host, port);
    }

    @SneakyThrows
    private void initEndpoints(@NotNull final String host, @NotNull final String port) {
        @NotNull val classes =
                new Reflections(ENDPOINTS_PACKAGE).getSubTypesOf(AbstractEndpoint.class);
        var isAbstract = false;
        for (@NotNull val clazz : classes) {
            @NotNull val endpointInstance =
                    clazz.getDeclaredConstructor(IEndpointLocator.class).newInstance(this);
            isAbstract = Modifier.isAbstract(clazz.getModifiers());
            if (isAbstract) continue;
            @NotNull val endpointName = endpointInstance.getClass().getSimpleName();
            @NotNull val address = "http://" + host + ":" + port + "/" + endpointName + "?wsdl";
            System.out.println(address);
            Endpoint.publish(address, endpointInstance);
        }
    }

    private void registerApplicationPid() {
        @NotNull val pidFileName = "task-manager.pid";
        @NotNull val pidFile = new File(pidFileName);
        @NotNull val pid = Long.toString(getApplicationPid());

        try {
            Files.write(Paths.get(pidFile.toURI()), pid.getBytes());
        } catch (@NotNull final IOException exception) {
            loggerService.error(exception);
        }

        pidFile.deleteOnExit();
    }

}
