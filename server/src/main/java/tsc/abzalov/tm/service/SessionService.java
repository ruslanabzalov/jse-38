package tsc.abzalov.tm.service;

import lombok.SneakyThrows;
import lombok.val;
import lombok.var;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import tsc.abzalov.tm.api.service.IConnectionService;
import tsc.abzalov.tm.api.service.ISessionService;
import tsc.abzalov.tm.api.service.IUserService;
import tsc.abzalov.tm.api.service.property.IHashingPropertyService;
import tsc.abzalov.tm.exception.auth.AccessDeniedException;
import tsc.abzalov.tm.exception.auth.IncorrectCredentialsException;
import tsc.abzalov.tm.exception.auth.SessionIsInactiveException;
import tsc.abzalov.tm.exception.auth.UserLockedException;
import tsc.abzalov.tm.interf.Executioner;
import tsc.abzalov.tm.model.Session;
import tsc.abzalov.tm.repository.SessionRepository;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.Date;

import static tsc.abzalov.tm.enumeration.Role.ADMIN;
import static tsc.abzalov.tm.util.HashUtil.hash;
import static tsc.abzalov.tm.util.SessionUtil.signSession;

public final class SessionService implements ISessionService {

    @NotNull
    private final IConnectionService connectionService;

    @NotNull
    private final Connection connection;

    @NotNull
    private final IUserService userService;

    @NotNull
    private final IHashingPropertyService propertyService;

    public SessionService(@NotNull final IConnectionService connectionService,
                          @NotNull final IUserService userService,
                          @NotNull final IHashingPropertyService propertyService) {
        this.connectionService = connectionService;
        this.connection = this.connectionService.getConnection();
        this.userService = userService;
        this.propertyService = propertyService;
    }


    @Override
    @NotNull
    @SneakyThrows
    public Session openSession(@Nullable final String login, @Nullable final String password) {
        if (login == null || password == null) throw new IncorrectCredentialsException();
        @Nullable val user = userService.findByLogin(login);
        if (user == null) throw new AccessDeniedException();
        if (user.isLocked()) throw new UserLockedException();

        @NotNull val counter = propertyService.getPasswordCounterProperty();
        @NotNull val salt = propertyService.getPasswordSaltProperty();

        if (user.getHashedPassword() == null) throw new IncorrectCredentialsException();
        if (!user.getHashedPassword().equals(hash(password, counter, salt))) throw new AccessDeniedException();

        @NotNull val session = new Session();
        session.setUserId(user.getId());
        session.setTimestamp(new Date().getTime());

        @NotNull val sessionRepository = new SessionRepository(this.connection);
        executeTransaction(() -> sessionRepository.addSession(session), this.connection);

        @Nullable val signedSession = sign(session);
        if (signedSession == null) throw new AccessDeniedException();
        return signedSession;
    }

    @Override
    @SneakyThrows
    public void closeSession(@Nullable final Session session) {
        if (session == null) throw new SessionIsInactiveException();
        session.setSignature(null);
        @NotNull val sessionRepository = new SessionRepository(this.connection);
        executeTransaction(() -> sessionRepository.removeSession(session.getId()), this.connection);
    }

    @Override
    @SneakyThrows
    public void validate(@Nullable final Session session) {
        if (session == null) throw new AccessDeniedException();
        if (session.getUserId() == null || session.getTimestamp() == null) throw new AccessDeniedException();
        if (userService.findById(session.getUserId()) == null) throw new AccessDeniedException();
        @Nullable var tempSession = session.clone();
        if (tempSession == null) throw new AccessDeniedException();
        tempSession.setSignature(null);
        tempSession = sign(tempSession);
        if (tempSession == null) throw new AccessDeniedException();
        if (!tempSession.getSignature().equals(session.getSignature())) throw new AccessDeniedException();
    }

    @Override
    @SneakyThrows
    public void validateAdminPermissions(@Nullable Session session, @NotNull IUserService userService) {
        validate(session);
        @Nullable val user = userService.findById(session.getUserId());
        if (user == null) throw new AccessDeniedException();
        if (!user.getRole().equals(ADMIN)) throw new AccessDeniedException();
    }

    @Override
    @NotNull
    @SneakyThrows
    public Session findSession(@Nullable Session session) {
        if (session == null) throw new AccessDeniedException();
        if (session.getUserId() == null || session.getTimestamp() == null) throw new AccessDeniedException();
        @Nullable val oldSign = session.getSignature();
        session.setSignature(null);
        @NotNull val sessionRepository = new SessionRepository(this.connection);
        @Nullable var foundedSession = sessionRepository.findSession(session.getId());
        if (foundedSession == null) throw new AccessDeniedException();
        session.setSignature(oldSign);
        foundedSession = sign(foundedSession);
        if (foundedSession == null) throw new AccessDeniedException();
        if (!foundedSession.equals(session)) throw new AccessDeniedException();
        return foundedSession;
    }

    @Nullable
    @SneakyThrows
    private Session sign(@NotNull final Session session) {
        return signSession(session, propertyService.getSessionCounterProperty(), propertyService.getSessionSaltProperty());
    }

    @SneakyThrows
    private void executeTransaction(@NotNull final Executioner executioner,
                                    @NotNull Connection connection) {
        try {
            executioner.execute();
            connection.commit();
        } catch (@NotNull final SQLException exception) {
            connection.rollback();
            throw new Exception();
        }
    }

}
