package tsc.abzalov.tm.service;

import lombok.SneakyThrows;
import lombok.val;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import tsc.abzalov.tm.api.service.IConnectionService;
import tsc.abzalov.tm.api.service.IProjectTaskService;
import tsc.abzalov.tm.exception.data.EmptyEntityException;
import tsc.abzalov.tm.exception.data.EmptyIdException;
import tsc.abzalov.tm.exception.data.EntityNotFoundException;
import tsc.abzalov.tm.model.Project;
import tsc.abzalov.tm.model.Task;
import tsc.abzalov.tm.repository.ProjectRepository;
import tsc.abzalov.tm.repository.TaskRepository;

import java.util.List;
import java.util.Optional;

public final class ProjectTaskService implements IProjectTaskService {

    @NotNull
    private final IConnectionService connectionService;

    public ProjectTaskService(@NotNull final IConnectionService connectionService) {
        this.connectionService = connectionService;
    }

    @Override
    @SneakyThrows
    public int indexOf(@Nullable Long userId, @Nullable Task task) {
        userId = Optional.ofNullable(userId).orElseThrow(EmptyIdException::new);
        task = Optional.ofNullable(task).orElseThrow(EmptyEntityException::new);

        @NotNull val taskRepository = new TaskRepository(this.connectionService.getConnection());
        return taskRepository.indexOf(userId, task);
    }

    @Override
    @SneakyThrows
    public boolean hasData(@Nullable Long userId) {
        userId = Optional.ofNullable(userId).orElseThrow(EmptyIdException::new);

        @NotNull val projectRepository = new ProjectRepository(this.connectionService.getConnection());
        @NotNull val taskRepository = new TaskRepository(this.connectionService.getConnection());
        return projectRepository.size(userId) != 0 && taskRepository.size(userId) != 0;
    }

    @Override
    @SneakyThrows
    public void addTaskToProjectById(@Nullable Long userId, @Nullable Long projectId, @Nullable Long taskId) {
        userId = Optional.ofNullable(userId).orElseThrow(EmptyIdException::new);
        projectId = Optional.ofNullable(projectId).orElseThrow(EmptyIdException::new);
        taskId = Optional.ofNullable(taskId).orElseThrow(EmptyIdException::new);

        @NotNull val taskRepository = new TaskRepository(this.connectionService.getConnection());
        taskRepository.addTaskToProjectById(userId, projectId, taskId);
    }

    @Override
    @Nullable
    @SneakyThrows
    public Project findProjectById(@Nullable Long id) {
        id = Optional.ofNullable(id).orElseThrow(EmptyIdException::new);

        @NotNull val projectRepository = new ProjectRepository(this.connectionService.getConnection());
        @Nullable val searchedProject = projectRepository.findById(id);
        return Optional.ofNullable(searchedProject).orElseThrow(EntityNotFoundException::new);
    }

    @Override
    @Nullable
    @SneakyThrows
    public Task findTaskById(@Nullable Long id) {
        id = Optional.ofNullable(id).orElseThrow(EmptyIdException::new);

        @NotNull val taskRepository = new TaskRepository(this.connectionService.getConnection());
        @Nullable val searchedTask = taskRepository.findById(id);
        return Optional.ofNullable(searchedTask).orElseThrow(EntityNotFoundException::new);
    }

    @Override
    @NotNull
    @SneakyThrows
    public List<Task> findProjectTasksById(@Nullable Long userId, @Nullable Long projectId) {
        userId = Optional.ofNullable(userId).orElseThrow(EmptyIdException::new);
        projectId = Optional.ofNullable(projectId).orElseThrow(EmptyIdException::new);

        @NotNull val taskRepository = new TaskRepository(this.connectionService.getConnection());
        return taskRepository.findProjectTasksById(userId, projectId);
    }

    @Override
    @SneakyThrows
    public void deleteProjectById(@Nullable Long id) {
        id = Optional.ofNullable(id).orElseThrow(EmptyIdException::new);

        @NotNull val projectRepository = new ProjectRepository(this.connectionService.getConnection());
        projectRepository.removeById(id);
    }

    @Override
    @SneakyThrows
    public void deleteProjectTasksById(@Nullable Long userId, @Nullable Long projectId) {
        userId = Optional.ofNullable(userId).orElseThrow(EmptyIdException::new);
        projectId = Optional.ofNullable(projectId).orElseThrow(EmptyIdException::new);

        @NotNull val taskRepository = new TaskRepository(this.connectionService.getConnection());
        taskRepository.deleteProjectTasksById(userId, projectId);
    }

    @Override
    @SneakyThrows
    public void deleteProjectTaskById(@Nullable Long userId, @Nullable Long projectId) {
        userId = Optional.ofNullable(userId).orElseThrow(EmptyIdException::new);
        projectId = Optional.ofNullable(projectId).orElseThrow(EmptyIdException::new);

        @NotNull val taskRepository = new TaskRepository(this.connectionService.getConnection());
        taskRepository.deleteProjectTaskById(userId, projectId);
    }

}
