package tsc.abzalov.tm.service;

import lombok.SneakyThrows;
import lombok.val;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import tsc.abzalov.tm.interf.Executioner;
import tsc.abzalov.tm.api.IBusinessEntityService;
import tsc.abzalov.tm.api.service.IConnectionService;
import tsc.abzalov.tm.exception.data.*;
import tsc.abzalov.tm.model.AbstractBusinessEntity;
import tsc.abzalov.tm.model.Project;
import tsc.abzalov.tm.model.Task;
import tsc.abzalov.tm.repository.AbstractBusinessEntityRepository;
import tsc.abzalov.tm.repository.ProjectRepository;
import tsc.abzalov.tm.repository.TaskRepository;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static tsc.abzalov.tm.util.InputUtil.isIndexIncorrect;
import static tsc.abzalov.tm.util.LiteralConst.DEFAULT_DESCRIPTION;

// TODO: Подумать над рефакторингом кода.
public abstract class AbstractBusinessEntityService<T extends AbstractBusinessEntity>
        extends AbstractService<T> implements IBusinessEntityService<T> {

    @NotNull
    private final IConnectionService connectionService;

    @NotNull
    private final Connection connection;

    public AbstractBusinessEntityService(@NotNull final IConnectionService connectionService) {
        this.connectionService = connectionService;
        this.connection = this.connectionService.getConnection();
    }

    @Override
    public long size() {
        if (this instanceof ProjectService) {
            @NotNull val projectRepository = new ProjectRepository(this.connection);
            return projectRepository.size();
        } else {
            @NotNull val taskRepository = new TaskRepository(this.connection);
            return taskRepository.size();
        }
    }

    @Override
    public boolean isEmpty() {
        if (this instanceof ProjectService) {
            @NotNull val projectRepository = new ProjectRepository(this.connection);
            return projectRepository.isEmpty();
        } else {
            @NotNull val taskRepository = new TaskRepository(this.connection);
            return taskRepository.isEmpty();
        }
    }

    @Override
    @SneakyThrows
    public void create(@Nullable final T entity) {
        if (entity == null) throw new EntityNotFoundException();

        if (this instanceof ProjectService) {
            @NotNull val projectRepository = new ProjectRepository(this.connection);
            executeTransaction(() -> projectRepository.create((Project) entity), this.connection);
        } else {
            @NotNull val taskRepository = new TaskRepository(connection);
            executeTransaction(() -> taskRepository.create((Task) entity), this.connection);
        }
    }

    @Override
    @SneakyThrows
    public void addAll(@Nullable final List<T> entities) {
        if (entities == null) throw new EmptyEntityException();
        entities.forEach(this::create);
    }

    @NotNull
    @Override
    public List<T> findAll() {
        if (this instanceof ProjectService) {
            @NotNull val projectRepository = new ProjectRepository(this.connection);
            return (List<T>) projectRepository.findAll();
        } else {
            @NotNull val taskRepository = new TaskRepository(this.connection);
            return (List<T>) taskRepository.findAll();
        }
    }

    @Nullable
    @Override
    @SneakyThrows
    public T findById(@Nullable final Long id) {
        if (id == null) throw new EmptyIdException();
        if (this instanceof ProjectService) {
            @NotNull val projectRepository = new ProjectRepository(this.connection);
            return Optional
                    .ofNullable((T) projectRepository.findById(id))
                    .orElseThrow(EntityNotFoundException::new);
        } else {
            @NotNull val taskRepository = new TaskRepository(this.connection);
            return Optional
                    .ofNullable((T) taskRepository.findById(id))
                    .orElseThrow(EntityNotFoundException::new);
        }
    }

    @Override
    public void clear() {
        if (this instanceof ProjectService) {
            @NotNull val projectRepository = new ProjectRepository(this.connection);
            executeTransaction(projectRepository::clear, this.connection);
        } else {
            @NotNull val taskRepository = (AbstractBusinessEntityRepository<Task>) new TaskRepository(this.connection);
            executeTransaction(taskRepository::clear, this.connection);
        }
    }

    @Override
    @SneakyThrows
    public void removeById(@Nullable final Long id) {
        if (id == null) throw new EmptyIdException();

        if (this instanceof ProjectService) {
            @NotNull val projectRepository = new ProjectRepository(this.connection);
            executeTransaction(() -> projectRepository.removeById(id), this.connection);
        } else {
            @NotNull val taskRepository = new TaskRepository(this.connection);
            executeTransaction(() -> taskRepository.removeById(id), this.connection);
        }
    }

    @Override
    @SneakyThrows
    public long size(@Nullable Long userId) {
        userId = Optional.ofNullable(userId).orElseThrow(EmptyIdException::new);

        if (this instanceof ProjectService) {
            @NotNull val projectRepository = new ProjectRepository(this.connection);
            return projectRepository.size(userId);
        } else {
            @NotNull val taskRepository = new TaskRepository(this.connection);
            return taskRepository.size(userId);
        }
    }

    @Override
    @SneakyThrows
    public boolean isEmpty(@Nullable Long userId) {
        userId = Optional.ofNullable(userId).orElseThrow(EmptyIdException::new);

        if (this instanceof ProjectService) {
            @NotNull val projectRepository = new ProjectRepository(this.connection);
            return projectRepository.isEmpty(userId);
        } else {
            @NotNull val taskRepository = new TaskRepository(this.connection);
            return taskRepository.isEmpty(userId);
        }
    }

    @Override
    @SneakyThrows
    public int indexOf(@Nullable Long userId, @Nullable T entity) {
        userId = Optional.ofNullable(userId).orElseThrow(EmptyIdException::new);
        entity = Optional.ofNullable(entity).orElseThrow(EmptyEntityException::new);

        @NotNull ArrayList<? extends AbstractBusinessEntity> entities;
        if (this instanceof ProjectService) {
            @NotNull val projectRepository = new ProjectRepository(this.connection);
            return projectRepository.indexOf(userId, (Project) entity);
        } else {
            @NotNull val taskRepository = new TaskRepository(this.connection);
            return taskRepository.indexOf(userId, (Task) entity);
        }
    }

    @Override
    @NotNull
    @SneakyThrows
    public List<T> findAll(@Nullable Long userId) {
        userId = Optional.ofNullable(userId).orElseThrow(EmptyIdException::new);

        if (this instanceof ProjectService) {
            @NotNull val projectRepository = new ProjectRepository(this.connection);
            return Optional
                    .ofNullable((List<T>) projectRepository.findAll(userId))
                    .orElseThrow(EntityNotFoundException::new);
        } else {
            @NotNull val taskRepository = new TaskRepository(this.connection);
            return Optional
                    .ofNullable((List<T>) taskRepository.findAll(userId))
                    .orElseThrow(EntityNotFoundException::new);
        }
    }

    @Override
    @Nullable
    @SneakyThrows
    public T findByIndex(@Nullable Long userId, int index) {
        userId = Optional.ofNullable(userId).orElseThrow(EmptyIdException::new);

        index = index - 1;
        if (isIndexIncorrect(index, size(userId))) throw new IncorrectIndexException(index);

        if (this instanceof ProjectService) {
            @NotNull val projectRepository = new ProjectRepository(this.connection);
            return Optional
                    .ofNullable((T) projectRepository.findByIndex(userId, index))
                    .orElseThrow(EntityNotFoundException::new);
        } else {
            @NotNull val taskRepository = new TaskRepository(this.connection);
            return Optional
                    .ofNullable((T) taskRepository.findByIndex(userId, index))
                    .orElseThrow(EntityNotFoundException::new);
        }
    }

    @Override
    @Nullable
    @SneakyThrows
    public T findByName(@Nullable Long userId, @Nullable String name) {
        userId = Optional.ofNullable(userId).orElseThrow(EmptyIdException::new);
        name = Optional.ofNullable(name).orElseThrow(EmptyNameException::new);

        if (this instanceof ProjectService) {
            @NotNull val projectRepository = new ProjectRepository(this.connection);
            return Optional
                    .ofNullable((T) projectRepository.findByName(userId, name))
                    .orElseThrow(EntityNotFoundException::new);
        } else {
            @NotNull val taskRepository = new TaskRepository(this.connection);
            return Optional
                    .ofNullable((T) taskRepository.findByName(userId, name))
                    .orElseThrow(EntityNotFoundException::new);
        }
    }

    @Override
    @Nullable
    @SneakyThrows
    public T editById(@Nullable final Long id, @Nullable final String name,
                      @Nullable final String description) {
        if (id == null) throw new EmptyIdException();
        if (name == null) throw new EmptyNameException();
        @NotNull val correctDescription = (description == null) ? DEFAULT_DESCRIPTION : description;

        if (this instanceof ProjectService) {
            @NotNull val projectRepository = new ProjectRepository(this.connection);
            executeTransaction(() -> projectRepository.editById(id, name, correctDescription), this.connection);
        } else {
            @NotNull val taskRepository = new TaskRepository(this.connection);
            executeTransaction(() -> taskRepository.editById(id, name, correctDescription), this.connection);
        }

        return findById(id);
    }

    @Override
    @Nullable
    @SneakyThrows
    public T editByName(@Nullable final Long userId, @Nullable final String name,
                        @Nullable final String description) {
        if (userId == null) throw new EmptyIdException();
        if (name == null) throw new EmptyNameException();
        @NotNull val correctDescription = (description == null) ? DEFAULT_DESCRIPTION : description;

        if (this instanceof ProjectService) {
            @NotNull val projectRepository = new ProjectRepository(this.connection);
            executeTransaction(() -> projectRepository.editByName(userId, name, correctDescription), this.connection);
        } else {
            @NotNull val taskRepository = new TaskRepository(this.connection);
            executeTransaction(() -> taskRepository.editByName(userId, name, correctDescription), this.connection);
        }

        return findByName(userId, name);
    }

    @Override
    @SneakyThrows
    public void clear(@Nullable final Long userId) {
        if (userId == null) throw new EmptyIdException();

        if (this instanceof ProjectService) {
            @NotNull val projectRepository = new ProjectRepository(this.connection);
            executeTransaction(() -> projectRepository.clear(userId), this.connection);
        } else {
            @NotNull val taskRepository = new TaskRepository(this.connection);
            executeTransaction(() -> taskRepository.clear(userId), this.connection);
        }
    }

    @Override
    @SneakyThrows
    public void removeByName(@Nullable final Long userId, @Nullable final String name) {
        if (userId == null) throw new EmptyIdException();
        if (name == null) throw new EmptyNameException();

        @NotNull val connection = this.connectionService.getConnection();
        if (this instanceof ProjectService) {
            @NotNull val projectRepository = new ProjectRepository(this.connection);
            executeTransaction(() -> projectRepository.removeByName(userId, name), this.connection);
        } else {
            @NotNull val taskRepository = new TaskRepository(this.connection);
            executeTransaction(() -> taskRepository.removeByName(userId, name), this.connection);
        }
    }

    @Override
    @Nullable
    @SneakyThrows
    public T startById(@Nullable final Long id) {
        if (id == null) throw new EmptyIdException();

        if (this instanceof ProjectService) {
            @NotNull val projectRepository = new ProjectRepository(this.connection);
            projectRepository.startById(id);
        } else {
            @NotNull val taskRepository = new TaskRepository(this.connection);
            taskRepository.startById(id);
        }

        return findById(id);
    }

    @Override
    @Nullable
    @SneakyThrows
    public T endById(@Nullable final Long id) {
        if (id == null) throw new EmptyIdException();

        if (this instanceof ProjectService) {
            @NotNull val projectRepository = new ProjectRepository(this.connection);
            projectRepository.endById(id);
        } else {
            @NotNull val taskRepository = new TaskRepository(this.connection);
            taskRepository.endById(id);
        }

        return findById(id);
    }

    @Override
    @NotNull
    @SneakyThrows
    public List<T> sortByName(@Nullable Long userId) {
        if (userId == null) throw new EmptyIdException();

        if (this instanceof ProjectService) {
            @NotNull val projectRepository = new ProjectRepository(this.connection);
            return (List<T>) projectRepository.sortByName(userId);
        } else {
            @NotNull val taskRepository = new TaskRepository(this.connection);
            return (List<T>) taskRepository.sortByName(userId);
        }
    }

    @Override
    @NotNull
    @SneakyThrows
    public List<T> sortByStartDate(@Nullable Long userId) {
        if (userId == null) throw new EmptyIdException();

        if (this instanceof ProjectService) {
            @NotNull val projectRepository = new ProjectRepository(this.connection);
            return (List<T>) projectRepository.sortByStartDate(userId);
        } else {
            @NotNull val taskRepository = new TaskRepository(this.connection);
            return (List<T>) taskRepository.sortByStartDate(userId);
        }
    }

    @Override
    @NotNull
    @SneakyThrows
    public List<T> sortByEndDate(@Nullable Long userId) {
        if (userId == null) throw new EmptyIdException();

        if (this instanceof ProjectService) {
            @NotNull val projectRepository = new ProjectRepository(this.connection);
            return (List<T>) projectRepository.sortByEndDate(userId);
        } else {
            @NotNull val taskRepository = new TaskRepository(this.connection);
            return (List<T>) taskRepository.sortByEndDate(userId);
        }
    }

    @Override
    @NotNull
    @SneakyThrows
    public List<T> sortByStatus(@Nullable Long userId) {
        if (userId == null) throw new EmptyIdException();

        if (this instanceof ProjectService) {
            @NotNull val projectRepository = new ProjectRepository(this.connection);
            return (List<T>) projectRepository.sortByStatus(userId);
        } else {
            @NotNull val taskRepository = new TaskRepository(this.connection);
            return (List<T>) taskRepository.sortByStatus(userId);
        }
    }

    @SneakyThrows
    private void executeTransaction(@NotNull final Executioner executioner,
                                    @NotNull Connection connection) {
        try {
            executioner.execute();
            connection.commit();
        } catch (@NotNull final SQLException exception) {
            connection.rollback();
            throw new Exception();
        }
    }

}
