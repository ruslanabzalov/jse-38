package tsc.abzalov.tm.endpoint;

import lombok.SneakyThrows;
import lombok.val;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import tsc.abzalov.tm.api.endpoint.IEndpointLocator;
import tsc.abzalov.tm.api.endpoint.ITaskEndpoint;
import tsc.abzalov.tm.exception.auth.AccessDeniedException;
import tsc.abzalov.tm.model.Task;
import tsc.abzalov.tm.model.Session;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.List;

@WebService
public final class TaskEndpoint extends AbstractEndpoint implements ITaskEndpoint {

    public TaskEndpoint() {
    }

    public TaskEndpoint(@WebParam(name = "session") @NotNull final IEndpointLocator endpointLocator) {
        super(endpointLocator);
    }

    @Override
    @WebMethod
    @SneakyThrows
    public long sizeTasks(@WebParam(name = "session") @Nullable Session session) {
        if (getEndpointLocator() == null) throw new AccessDeniedException();
        getEndpointLocator().getSessionService().validate(session);
        @NotNull val taskService = getEndpointLocator().getTaskService();
        return taskService.size();
    }

    @Override
    @WebMethod
    @SneakyThrows
    public boolean isEmptyTaskList(@WebParam(name = "session") @Nullable Session session) {
        if (getEndpointLocator() == null) throw new AccessDeniedException();
        getEndpointLocator().getSessionService().validate(session);
        @NotNull val taskService = getEndpointLocator().getTaskService();
        return taskService.isEmpty();
    }

    @Override
    @WebMethod
    @SneakyThrows
    public void createTask(@WebParam(name = "session") @Nullable Session session,
                           @WebParam(name = "name") @NotNull String name,
                           @WebParam(name = "description") @NotNull String description) {
        if (getEndpointLocator() == null) throw new AccessDeniedException();
        getEndpointLocator().getSessionService().validate(session);
        @NotNull val task = new Task();
        task.setName(name);
        task.setDescription(description);
        task.setUserId(session.getUserId());
        @NotNull val taskService = getEndpointLocator().getTaskService();
        taskService.create(task);
    }

    @Override
    @WebMethod
    @SneakyThrows
    public void addAllTasks(@WebParam(name = "session") @Nullable Session session,
                            @WebParam(name = "tasks") @Nullable List<Task> tasks) {
        if (getEndpointLocator() == null) throw new AccessDeniedException();
        getEndpointLocator().getSessionService().validate(session);
        @NotNull val taskService = getEndpointLocator().getTaskService();
        taskService.addAll(tasks);
    }

    @Override
    @Nullable
    @WebMethod
    @SneakyThrows
    public Task findTasksById(@WebParam(name = "session") @Nullable Session session,
                              @WebParam(name = "id") @Nullable Long id) {
        if (getEndpointLocator() == null) throw new AccessDeniedException();
        getEndpointLocator().getSessionService().validate(session);
        @NotNull val taskService = getEndpointLocator().getTaskService();
        return taskService.findById(id);
    }

    @Override
    @WebMethod
    @SneakyThrows
    public void clearAllTasks(@WebParam(name = "session") @Nullable Session session) {
        if (getEndpointLocator() == null) throw new AccessDeniedException();
        getEndpointLocator().getSessionService().validate(session);
        @NotNull val taskService = getEndpointLocator().getTaskService();
        taskService.clear();
    }

    @Override
    @WebMethod
    @SneakyThrows
    public long tasksSize(@WebParam(name = "session") @Nullable Session session) {
        if (getEndpointLocator() == null) throw new AccessDeniedException();
        getEndpointLocator().getSessionService().validate(session);
        @NotNull val taskService = getEndpointLocator().getTaskService();
        return taskService.size(session.getUserId());
    }

    @Override
    @WebMethod
    @SneakyThrows
    public boolean areTasksEmpty(@WebParam(name = "session") @Nullable Session session) {
        if (getEndpointLocator() == null) throw new AccessDeniedException();
        getEndpointLocator().getSessionService().validate(session);
        @NotNull val taskService = getEndpointLocator().getTaskService();
        return taskService.isEmpty(session.getUserId());
    }

    @Override
    @WebMethod
    @SneakyThrows

    public int taskIndex(@WebParam(name = "session") @Nullable Session session,
                            @WebParam(name = "entity") @Nullable Task entity) {
        if (getEndpointLocator() == null) throw new AccessDeniedException();
        getEndpointLocator().getSessionService().validate(session);
        @NotNull val taskService = getEndpointLocator().getTaskService();
        return taskService.indexOf(session.getUserId(), entity);
    }

    @NotNull
    @Override
    @WebMethod
    @SneakyThrows

    public List<Task> findAllTasks(@WebParam(name = "session") @Nullable Session session) {
        if (getEndpointLocator() == null) throw new AccessDeniedException();
        getEndpointLocator().getSessionService().validate(session);
        @NotNull val taskService = getEndpointLocator().getTaskService();
        return taskService.findAll(session.getUserId());
    }

    @Override
    @Nullable
    @WebMethod
    @SneakyThrows

    public Task findTaskById(@WebParam(name = "session") @Nullable Session session,
                                   @WebParam(name = "id") @Nullable Long id) {
        if (getEndpointLocator() == null) throw new AccessDeniedException();
        getEndpointLocator().getSessionService().validate(session);
        @NotNull val taskService = getEndpointLocator().getTaskService();
        return taskService.findById(id);
    }

    @Override
    @Nullable
    @WebMethod
    @SneakyThrows

    public Task findTaskByIndex(@WebParam(name = "session") @Nullable Session session,
                                      @WebParam(name = "index") int index) {
        if (getEndpointLocator() == null) throw new AccessDeniedException();
        getEndpointLocator().getSessionService().validate(session);
        @NotNull val taskService = getEndpointLocator().getTaskService();
        return taskService.findByIndex(session.getUserId(), index);
    }

    @Override
    @Nullable
    @WebMethod
    @SneakyThrows

    public Task findTaskByName(@WebParam(name = "session") @Nullable Session session,
                                     @WebParam(name = "name") @Nullable String name) {
        if (getEndpointLocator() == null) throw new AccessDeniedException();
        getEndpointLocator().getSessionService().validate(session);
        @NotNull val taskService = getEndpointLocator().getTaskService();
        return taskService.findByName(session.getUserId(), name);
    }

    @Override
    @Nullable
    @WebMethod
    @SneakyThrows

    public Task editTaskById(@WebParam(name = "session") @Nullable Session session,
                                   @WebParam(name = "id") @Nullable Long id,
                                   @WebParam(name = "name") @Nullable String name,
                                   @WebParam(name = "description") @Nullable String description) {
        if (getEndpointLocator() == null) throw new AccessDeniedException();
        getEndpointLocator().getSessionService().validate(session);
        @NotNull val taskService = getEndpointLocator().getTaskService();
        return taskService.editById(id, name, description);
    }

    @Override
    @Nullable
    @WebMethod
    @SneakyThrows

    public Task editTaskByName(@WebParam(name = "session") @Nullable Session session,
                                     @WebParam(name = "name") @Nullable String name,
                                     @WebParam(name = "description") @Nullable String description) {
        if (getEndpointLocator() == null) throw new AccessDeniedException();
        getEndpointLocator().getSessionService().validate(session);
        @NotNull val taskService = getEndpointLocator().getTaskService();
        return taskService.editByName(session.getUserId(), name, description);
    }

    @Override
    @WebMethod
    @SneakyThrows
    public void clearTasks(@WebParam(name = "session") @Nullable Session session) {
        if (getEndpointLocator() == null) throw new AccessDeniedException();
        getEndpointLocator().getSessionService().validate(session);
        @NotNull val taskService = getEndpointLocator().getTaskService();
        taskService.clear(session.getUserId());
    }

    @Override
    @WebMethod
    @SneakyThrows

    public void removeTaskById(@WebParam(name = "session") @Nullable Session session,
                                  @WebParam(name = "id") @Nullable Long id) {
        if (getEndpointLocator() == null) throw new AccessDeniedException();
        getEndpointLocator().getSessionService().validate(session);
        @NotNull val taskService = getEndpointLocator().getTaskService();
        taskService.removeById(id);
    }

    @Override
    @WebMethod
    @SneakyThrows

    public void removeTaskByName(@WebParam(name = "session") @Nullable Session session,
                                    @WebParam(name = "name") @Nullable String name) {
        if (getEndpointLocator() == null) throw new AccessDeniedException();
        getEndpointLocator().getSessionService().validate(session);
        @NotNull val taskService = getEndpointLocator().getTaskService();
        taskService.removeByName(session.getUserId(), name);
    }

    @Override
    @Nullable
    @WebMethod
    @SneakyThrows

    public Task startTaskById(@WebParam(name = "session") @Nullable Session session,
                                    @WebParam(name = "id") @Nullable Long id) {
        if (getEndpointLocator() == null) throw new AccessDeniedException();
        getEndpointLocator().getSessionService().validate(session);
        @NotNull val taskService = getEndpointLocator().getTaskService();
        return taskService.startById(id);
    }

    @Override
    @Nullable
    @WebMethod
    @SneakyThrows

    public Task endTaskById(@WebParam(name = "session") @Nullable Session session,
                                  @WebParam(name = "id") @Nullable Long id) {
        if (getEndpointLocator() == null) throw new AccessDeniedException();
        getEndpointLocator().getSessionService().validate(session);
        @NotNull val taskService = getEndpointLocator().getTaskService();
        return taskService.endById(id);
    }

    @NotNull
    @Override
    @WebMethod
    @SneakyThrows

    public List<Task> sortTasksByName(@WebParam(name = "session") @Nullable Session session) {
        if (getEndpointLocator() == null) throw new AccessDeniedException();
        getEndpointLocator().getSessionService().validate(session);
        @NotNull val taskService = getEndpointLocator().getTaskService();
        return taskService.sortByName(session.getUserId());
    }

    @NotNull
    @Override
    @WebMethod
    @SneakyThrows

    public List<Task> sortTasksByStartDate(@WebParam(name = "session") @Nullable Session session) {
        if (getEndpointLocator() == null) throw new AccessDeniedException();
        getEndpointLocator().getSessionService().validate(session);
        @NotNull val taskService = getEndpointLocator().getTaskService();
        return taskService.sortByStartDate(session.getUserId());
    }

    @NotNull
    @Override
    @WebMethod
    @SneakyThrows

    public List<Task> sortTasksByEndDate(@WebParam(name = "session") @Nullable Session session) {
        if (getEndpointLocator() == null) throw new AccessDeniedException();
        getEndpointLocator().getSessionService().validate(session);
        @NotNull val taskService = getEndpointLocator().getTaskService();
        return taskService.sortByEndDate(session.getUserId());
    }

    @NotNull
    @Override
    @WebMethod
    @SneakyThrows

    public List<Task> sortTasksByStatus(@WebParam(name = "session") @Nullable Session session) {
        if (getEndpointLocator() == null) throw new AccessDeniedException();
        getEndpointLocator().getSessionService().validate(session);
        @NotNull val taskService = getEndpointLocator().getTaskService();
        return taskService.sortByStatus(session.getUserId());
    }
    
}
