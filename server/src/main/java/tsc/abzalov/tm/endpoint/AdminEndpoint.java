package tsc.abzalov.tm.endpoint;

import lombok.SneakyThrows;
import lombok.val;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import tsc.abzalov.tm.api.endpoint.IAdminEndpoint;
import tsc.abzalov.tm.api.endpoint.IEndpointLocator;
import tsc.abzalov.tm.enumeration.Role;
import tsc.abzalov.tm.exception.auth.AccessDeniedException;
import tsc.abzalov.tm.model.Session;
import tsc.abzalov.tm.model.User;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.List;

@WebService
public final class AdminEndpoint extends AbstractEndpoint implements IAdminEndpoint {

    public AdminEndpoint() {
    }

    public AdminEndpoint(@NotNull final IEndpointLocator endpointLocator) {
        super(endpointLocator);
    }

    @Override
    @WebMethod
    @SneakyThrows
    public void adminCreateUserWithCustomRole(@WebParam(name = "session") @Nullable Session session,
                                              @WebParam(name = "login") @Nullable String login,
                                              @WebParam(name = "password") @Nullable String password,
                                              @WebParam(name = "role") @Nullable Role role,
                                              @WebParam(name = "firstName") @Nullable String firstName,
                                              @WebParam(name = "lastName") @Nullable String lastName,
                                              @WebParam(name = "email") @Nullable String email) {
        if (getEndpointLocator() == null) throw new AccessDeniedException();
        @NotNull val userService = getEndpointLocator().getUserService();
        @NotNull val sessionService = getEndpointLocator().getSessionService();
        sessionService.validateAdminPermissions(session, userService);
        userService.create(login, password, role, firstName, lastName, email);
    }

    @Override
    @WebMethod
    @SneakyThrows
    public void adminDeleteUserByLogin(@WebParam(name = "session") @Nullable Session session,
                                       @WebParam(name = "login") @Nullable String login) {
        if (getEndpointLocator() == null) throw new AccessDeniedException();
        @NotNull val userService = getEndpointLocator().getUserService();
        @NotNull val sessionService = getEndpointLocator().getSessionService();
        sessionService.validateAdminPermissions(session, userService);
        userService.deleteByLogin(login);
    }

    @Nullable
    @Override
    @WebMethod
    @SneakyThrows
    public User adminLockUnlockUserById(@WebParam(name = "session") @Nullable Session session,
                                        @WebParam(name = "id") @Nullable Long id) {
        if (getEndpointLocator() == null) throw new AccessDeniedException();
        @NotNull val userService = getEndpointLocator().getUserService();
        @NotNull val sessionService = getEndpointLocator().getSessionService();
        sessionService.validateAdminPermissions(session, userService);
//        return userService.lockUnlockById(id);
        return new User();
    }

    @Nullable
    @Override
    @WebMethod
    @SneakyThrows
    public User adminLockUnlockUserByLogin(@WebParam(name = "session") @Nullable Session session,
                                           @WebParam(name = "login") @Nullable String login) {
        if (getEndpointLocator() == null) throw new AccessDeniedException();
        @NotNull val userService = getEndpointLocator().getUserService();
        @NotNull val sessionService = getEndpointLocator().getSessionService();
        sessionService.validateAdminPermissions(session, userService);
//        return userService.lockUnlockByLogin(login);
        return new User();
    }

    @Override
    @WebMethod
    @SneakyThrows
    public long adminSizeUsers(@WebParam(name = "session") @Nullable Session session) {
        if (getEndpointLocator() == null) throw new AccessDeniedException();
        @NotNull val userService = getEndpointLocator().getUserService();
        @NotNull val sessionService = getEndpointLocator().getSessionService();
        sessionService.validateAdminPermissions(session, userService);
        return userService.size();
    }

    @Override
    @WebMethod
    @SneakyThrows
    public boolean adminIsEmptyUserList(@WebParam(name = "session") @Nullable Session session) {
        if (getEndpointLocator() == null) throw new AccessDeniedException();
        @NotNull val userService = getEndpointLocator().getUserService();
        @NotNull val sessionService = getEndpointLocator().getSessionService();
        sessionService.validateAdminPermissions(session, userService);
        return userService.isEmpty();
    }

    @Override
    @WebMethod
    @SneakyThrows
    public void adminCreateUserWithEntity(@WebParam(name = "session") @Nullable Session session,
                                          @WebParam(name = "user") @Nullable User user) {
        if (getEndpointLocator() == null) throw new AccessDeniedException();
        @NotNull val userService = getEndpointLocator().getUserService();
        @NotNull val sessionService = getEndpointLocator().getSessionService();
        sessionService.validateAdminPermissions(session, userService);
        userService.create(user);
    }

    @Override
    @WebMethod
    @SneakyThrows
    public void adminAddAllUsers(@WebParam(name = "session") @Nullable Session session,
                                 @WebParam(name = "user") @Nullable List<User> users) {
        if (getEndpointLocator() == null) throw new AccessDeniedException();
        @NotNull val userService = getEndpointLocator().getUserService();
        @NotNull val sessionService = getEndpointLocator().getSessionService();
        sessionService.validateAdminPermissions(session, userService);
        userService.addAll(users);
    }

    @NotNull
    @Override
    @WebMethod
    @SneakyThrows
    public List<User> adminFindAllUsers(@WebParam(name = "session") @Nullable Session session) {
        if (getEndpointLocator() == null) throw new AccessDeniedException();
        @NotNull val userService = getEndpointLocator().getUserService();
        @NotNull val sessionService = getEndpointLocator().getSessionService();
        sessionService.validateAdminPermissions(session, userService);
        return userService.findAll();
    }

    @Override
    @Nullable
    @WebMethod
    @SneakyThrows
    public User adminFindUsersById(@WebParam(name = "session") @Nullable Session session,
                                   @WebParam(name = "id") @Nullable Long id) {
        if (getEndpointLocator() == null) throw new AccessDeniedException();
        @NotNull val userService = getEndpointLocator().getUserService();
        @NotNull val sessionService = getEndpointLocator().getSessionService();
        sessionService.validateAdminPermissions(session, userService);
        return userService.findById(id);
    }

    @Override
    @WebMethod
    @SneakyThrows
    public void adminClearAllUsers(@WebParam(name = "session") @Nullable Session session) {
        if (getEndpointLocator() == null) throw new AccessDeniedException();
        @NotNull val userService = getEndpointLocator().getUserService();
        @NotNull val sessionService = getEndpointLocator().getSessionService();
        sessionService.validateAdminPermissions(session, userService);
        userService.clear();
    }

    @Override
    @WebMethod
    @SneakyThrows
    public void adminRemoveUserById(@WebParam(name = "session") @Nullable Session session,
                                    @WebParam(name = "id") @Nullable Long id) {
        if (getEndpointLocator() == null) throw new AccessDeniedException();
        @NotNull val userService = getEndpointLocator().getUserService();
        @NotNull val sessionService = getEndpointLocator().getSessionService();
        sessionService.validateAdminPermissions(session, userService);
        userService.removeById(id);
    }

    @Override
    @WebMethod
    @SneakyThrows
    public void adminCreateUser(@WebParam(name = "session") @Nullable Session session,
                                @WebParam(name = "login") @Nullable final String login,
                                @WebParam(name = "password") @Nullable final String password,
                                @WebParam(name = "firstName") @Nullable final String firstName,
                                @WebParam(name = "lastName") @Nullable final String lastName,
                                @WebParam(name = "email") @Nullable final String email) {
        if (getEndpointLocator() == null) throw new AccessDeniedException();
        @NotNull val userService = getEndpointLocator().getUserService();
        @NotNull val sessionService = getEndpointLocator().getSessionService();
        sessionService.validateAdminPermissions(session, userService);
        userService.create(login, password, firstName, lastName, email);
    }

    @Override
    @WebMethod
    @SneakyThrows
    public boolean adminIsUserExist(@WebParam(name = "session") @Nullable final Session session,
                                    @WebParam(name = "login") @Nullable final String login,
                                    @WebParam(name = "email") @Nullable final String email) {
        if (getEndpointLocator() == null) throw new AccessDeniedException();
        @NotNull val userService = getEndpointLocator().getUserService();
        @NotNull val sessionService = getEndpointLocator().getSessionService();
        sessionService.validateAdminPermissions(session, userService);
        return userService.isUserExist(login, email);
    }

    @Override
    @Nullable
    @WebMethod
    @SneakyThrows

    public User adminFindUserUserById(@WebParam(name = "session") @Nullable Session session,
                                      @WebParam(name = "userId") @Nullable Long userId) {
        if (getEndpointLocator() == null) throw new AccessDeniedException();
        @NotNull val userService = getEndpointLocator().getUserService();
        @NotNull val sessionService = getEndpointLocator().getSessionService();
        sessionService.validateAdminPermissions(session, userService);
        return userService.findById(userId);
    }

    @Override
    @Nullable
    @WebMethod
    @SneakyThrows
    public User adminFindUserByLogin(@WebParam(name = "session") @Nullable final Session session,
                                     @WebParam(name = "login") @Nullable final String login) {
        if (getEndpointLocator() == null) throw new AccessDeniedException();
        @NotNull val userService = getEndpointLocator().getUserService();
        @NotNull val sessionService = getEndpointLocator().getSessionService();
        sessionService.validateAdminPermissions(session, userService);
        return userService.findByLogin(login);
    }

    @Override
    @Nullable
    @WebMethod
    @SneakyThrows
    public User adminEditPasswordById(@WebParam(name = "session") @Nullable final Session session,
                                      @WebParam(name = "id") @Nullable final Long userId,
                                      @WebParam(name = "password") @Nullable final String newPassword) {
        if (getEndpointLocator() == null) throw new AccessDeniedException();
        @NotNull val userService = getEndpointLocator().getUserService();
        @NotNull val sessionService = getEndpointLocator().getSessionService();
        sessionService.validateAdminPermissions(session, userService);
        return userService.editPasswordById(userId, newPassword);
    }

    @Override
    @Nullable
    @WebMethod
    @SneakyThrows
    public User adminEditUserInfoById(@WebParam(name = "session") @Nullable final Session session,
                                      @WebParam(name = "id") @Nullable final Long userId,
                                      @WebParam(name = "firstName") @Nullable final String firstName,
                                      @WebParam(name = "lastName") @Nullable final String lastName) {
        if (getEndpointLocator() == null) throw new AccessDeniedException();
        @NotNull val userService = getEndpointLocator().getUserService();
        @NotNull val sessionService = getEndpointLocator().getSessionService();
        sessionService.validateAdminPermissions(session, userService);
        return userService.editUserInfoById(userId, firstName, lastName);
    }

}
