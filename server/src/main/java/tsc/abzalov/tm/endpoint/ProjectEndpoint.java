package tsc.abzalov.tm.endpoint;

import lombok.SneakyThrows;
import lombok.val;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import tsc.abzalov.tm.api.endpoint.IEndpointLocator;
import tsc.abzalov.tm.api.endpoint.IProjectEndpoint;
import tsc.abzalov.tm.exception.auth.AccessDeniedException;
import tsc.abzalov.tm.model.Project;
import tsc.abzalov.tm.model.Session;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.List;

@WebService
public final class ProjectEndpoint extends AbstractEndpoint implements IProjectEndpoint {

    public ProjectEndpoint() {
    }

    public ProjectEndpoint(@NotNull final IEndpointLocator endpointLocator) {
        super(endpointLocator);
    }

    @Override
    @WebMethod
    @SneakyThrows
    public long sizeProjects(@WebParam(name = "session") @Nullable Session session) {
        if (getEndpointLocator() == null) throw new AccessDeniedException();
        getEndpointLocator().getSessionService().validate(session);
        @NotNull val projectService = getEndpointLocator().getProjectService();
        return projectService.size();
    }

    @Override
    @WebMethod
    @SneakyThrows
    public boolean isEmptyProjectList(@WebParam(name = "session") @Nullable Session session) {
        if (getEndpointLocator() == null) throw new AccessDeniedException();
        getEndpointLocator().getSessionService().validate(session);
        @NotNull val projectService = getEndpointLocator().getProjectService();
        return projectService.isEmpty();
    }

    @Override
    @WebMethod
    @SneakyThrows
    public void createProject(@WebParam(name = "session") @Nullable Session session,
                              @WebParam(name = "name") @NotNull String name,
                              @WebParam(name = "description") @NotNull String description) {
        if (getEndpointLocator() == null) throw new AccessDeniedException();
        getEndpointLocator().getSessionService().validate(session);
        @NotNull val project = new Project();
        project.setName(name);
        project.setDescription(description);
        project.setUserId(session.getUserId());
        @NotNull val projectService = getEndpointLocator().getProjectService();
        projectService.create(project);
    }

    @Override
    @WebMethod
    @SneakyThrows
    public void addAllProjects(@WebParam(name = "session") @Nullable Session session,
                               @WebParam(name = "projects") @Nullable List<Project> projects) {
        if (getEndpointLocator() == null) throw new AccessDeniedException();
        getEndpointLocator().getSessionService().validate(session);
        @NotNull val projectService = getEndpointLocator().getProjectService();
        projectService.addAll(projects);
    }

    @NotNull
    @Override
    @WebMethod
    @SneakyThrows
    public List<Project> findAllProjectsById(@WebParam(name = "session") @Nullable Session session) {
        if (getEndpointLocator() == null) throw new AccessDeniedException();
        getEndpointLocator().getSessionService().validate(session);
        @NotNull val projectService = getEndpointLocator().getProjectService();
        return projectService.findAll();
    }

    @Override
    @WebMethod
    @SneakyThrows
    public @Nullable Project findProjectsById(@WebParam(name = "session") @Nullable Session session,
                                              @WebParam(name = "id") @Nullable Long id) {
        if (getEndpointLocator() == null) throw new AccessDeniedException();
        getEndpointLocator().getSessionService().validate(session);
        @NotNull val projectService = getEndpointLocator().getProjectService();
        return projectService.findById(id);
    }

    @Override
    @WebMethod
    @SneakyThrows
    public void clearAllProjects(@WebParam(name = "session") @Nullable Session session) {
        if (getEndpointLocator() == null) throw new AccessDeniedException();
        getEndpointLocator().getSessionService().validate(session);
        @NotNull val projectService = getEndpointLocator().getProjectService();
        projectService.clear();
    }

    @Override
    @WebMethod
    @SneakyThrows
    public long projectsSize(@WebParam(name = "session") @Nullable Session session) {
        if (getEndpointLocator() == null) throw new AccessDeniedException();
        getEndpointLocator().getSessionService().validate(session);
        @NotNull val projectService = getEndpointLocator().getProjectService();
        return projectService.size(session.getUserId());
    }

    @Override
    @WebMethod
    @SneakyThrows
    public boolean areProjectsEmpty(@WebParam(name = "session") @Nullable Session session) {
        if (getEndpointLocator() == null) throw new AccessDeniedException();
        getEndpointLocator().getSessionService().validate(session);
        @NotNull val projectService = getEndpointLocator().getProjectService();
        return projectService.isEmpty(session.getUserId());
    }

    @Override
    @WebMethod
    @SneakyThrows

    public int projectIndex(@WebParam(name = "session") @Nullable Session session,
                            @WebParam(name = "entity") @Nullable Project entity) {
        if (getEndpointLocator() == null) throw new AccessDeniedException();
        getEndpointLocator().getSessionService().validate(session);
        @NotNull val projectService = getEndpointLocator().getProjectService();
        return projectService.indexOf(session.getUserId(), entity);
    }

    @NotNull
    @Override
    @WebMethod
    @SneakyThrows

    public List<Project> findAllProjects(@WebParam(name = "session") @Nullable Session session) {
        if (getEndpointLocator() == null) throw new AccessDeniedException();
        getEndpointLocator().getSessionService().validate(session);
        @NotNull val projectService = getEndpointLocator().getProjectService();
        return projectService.findAll(session.getUserId());
    }

    @Override
    @Nullable
    @WebMethod
    @SneakyThrows

    public Project findProjectById(@WebParam(name = "session") @Nullable Session session,
                                   @WebParam(name = "id") @Nullable Long id) {
        if (getEndpointLocator() == null) throw new AccessDeniedException();
        getEndpointLocator().getSessionService().validate(session);
        @NotNull val projectService = getEndpointLocator().getProjectService();
        return projectService.findById(id);
    }

    @Override
    @Nullable
    @WebMethod
    @SneakyThrows

    public Project findProjectByIndex(@WebParam(name = "session") @Nullable Session session,
                                      @WebParam(name = "index") int index) {
        if (getEndpointLocator() == null) throw new AccessDeniedException();
        getEndpointLocator().getSessionService().validate(session);
        @NotNull val projectService = getEndpointLocator().getProjectService();
        return projectService.findByIndex(session.getUserId(), index);
    }

    @Override
    @Nullable
    @WebMethod
    @SneakyThrows

    public Project findProjectByName(@WebParam(name = "session") @Nullable Session session,
                                     @WebParam(name = "name") @Nullable String name) {
        if (getEndpointLocator() == null) throw new AccessDeniedException();
        getEndpointLocator().getSessionService().validate(session);
        @NotNull val projectService = getEndpointLocator().getProjectService();
        return projectService.findByName(session.getUserId(), name);
    }

    @Override
    @Nullable
    @WebMethod
    @SneakyThrows

    public Project editProjectById(@WebParam(name = "session") @Nullable Session session,
                                   @WebParam(name = "id") @Nullable Long id,
                                   @WebParam(name = "name") @Nullable String name,
                                   @WebParam(name = "description") @Nullable String description) {
        if (getEndpointLocator() == null) throw new AccessDeniedException();
        getEndpointLocator().getSessionService().validate(session);
        @NotNull val projectService = getEndpointLocator().getProjectService();
        return projectService.editById(id, name, description);
    }

    @Override
    @Nullable
    @WebMethod
    @SneakyThrows

    public Project editProjectByName(@WebParam(name = "session") @Nullable Session session,
                                     @WebParam(name = "name") @Nullable String name,
                                     @WebParam(name = "description") @Nullable String description) {
        if (getEndpointLocator() == null) throw new AccessDeniedException();
        getEndpointLocator().getSessionService().validate(session);
        @NotNull val projectService = getEndpointLocator().getProjectService();
        return projectService.editByName(session.getUserId(), name, description);
    }

    @Override
    @WebMethod
    @SneakyThrows
    public void clearProjects(@WebParam(name = "session") @Nullable Session session) {
        if (getEndpointLocator() == null) throw new AccessDeniedException();
        getEndpointLocator().getSessionService().validate(session);
        @NotNull val projectService = getEndpointLocator().getProjectService();
        projectService.clear(session.getUserId());
    }

    @Override
    @WebMethod
    @SneakyThrows

    public void removeProjectById(@WebParam(name = "session") @Nullable Session session,
                                  @WebParam(name = "id") @Nullable Long id) {
        if (getEndpointLocator() == null) throw new AccessDeniedException();
        getEndpointLocator().getSessionService().validate(session);
        @NotNull val projectService = getEndpointLocator().getProjectService();
        projectService.removeById(id);
    }

    @Override
    @WebMethod
    @SneakyThrows

    public void removeProjectByName(@WebParam(name = "session") @Nullable Session session,
                                    @WebParam(name = "name") @Nullable String name) {
        if (getEndpointLocator() == null) throw new AccessDeniedException();
        getEndpointLocator().getSessionService().validate(session);
        @NotNull val projectService = getEndpointLocator().getProjectService();
        projectService.removeByName(session.getUserId(), name);
    }

    @Override
    @Nullable
    @WebMethod
    @SneakyThrows

    public Project startProjectById(@WebParam(name = "session") @Nullable Session session,
                                    @WebParam(name = "id") @Nullable Long id) {
        if (getEndpointLocator() == null) throw new AccessDeniedException();
        getEndpointLocator().getSessionService().validate(session);
        @NotNull val projectService = getEndpointLocator().getProjectService();
        return projectService.startById(id);
    }

    @Override
    @Nullable
    @WebMethod
    @SneakyThrows

    public Project endProjectById(@WebParam(name = "session") @Nullable Session session,
                                  @WebParam(name = "id") @Nullable Long id) {
        if (getEndpointLocator() == null) throw new AccessDeniedException();
        getEndpointLocator().getSessionService().validate(session);
        @NotNull val projectService = getEndpointLocator().getProjectService();
        return projectService.endById(id);
    }

    @NotNull
    @Override
    @WebMethod
    @SneakyThrows

    public List<Project> sortProjectsByName(@WebParam(name = "session") @Nullable Session session) {
        if (getEndpointLocator() == null) throw new AccessDeniedException();
        getEndpointLocator().getSessionService().validate(session);
        @NotNull val projectService = getEndpointLocator().getProjectService();
        return projectService.sortByName(session.getUserId());
    }

    @NotNull
    @Override
    @WebMethod
    @SneakyThrows

    public List<Project> sortProjectsByStartDate(@WebParam(name = "session") @Nullable Session session) {
        if (getEndpointLocator() == null) throw new AccessDeniedException();
        getEndpointLocator().getSessionService().validate(session);
        @NotNull val projectService = getEndpointLocator().getProjectService();
        return projectService.sortByStartDate(session.getUserId());
    }

    @NotNull
    @Override
    @WebMethod
    @SneakyThrows

    public List<Project> sortProjectsByEndDate(@WebParam(name = "session") @Nullable Session session) {
        if (getEndpointLocator() == null) throw new AccessDeniedException();
        getEndpointLocator().getSessionService().validate(session);
        @NotNull val projectService = getEndpointLocator().getProjectService();
        return projectService.sortByEndDate(session.getUserId());
    }

    @NotNull
    @Override
    @WebMethod
    @SneakyThrows

    public List<Project> sortProjectsByStatus(@WebParam(name = "session") @Nullable Session session) {
        if (getEndpointLocator() == null) throw new AccessDeniedException();
        getEndpointLocator().getSessionService().validate(session);
        @NotNull val projectService = getEndpointLocator().getProjectService();
        return projectService.sortByStatus(session.getUserId());
    }

}
