package tsc.abzalov.tm.repository;

import lombok.val;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import tsc.abzalov.tm.api.repository.ITaskRepository;
import tsc.abzalov.tm.model.Task;

import java.sql.Connection;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

public final class TaskRepository extends AbstractBusinessEntityRepository<Task>
        implements ITaskRepository {

    public TaskRepository(@NotNull final Connection connection) {
        super(connection);
    }

    @Nullable
    private List<Task> tasks;

    @Override
    public void addTaskToProjectById(@NotNull final Long userId, @NotNull final Long projectId,
                                     @NotNull final Long taskId) {
        tasks.stream()
                .filter(task -> {
                    @Nullable val currentUserId = task.getUserId();
                    @NotNull val currentTaskId = task.getId();
                    return userId.equals(currentUserId) && taskId.equals(currentTaskId);
                })
                .findFirst()
                .map(task -> {
                    task.setProjectId(projectId);
                    return task;
                });
    }

    @Override
    @NotNull
    public List<Task> findProjectTasksById(@NotNull final Long userId, @NotNull final Long projectId) {
        return tasks.stream()
                .filter(task -> {
                    @Nullable val currentUserId = task.getUserId();
                    return userId.equals(currentUserId) && isProjectTaskExist(projectId, task);
                })
                .collect(Collectors.toList());
    }

    @Override
    public void deleteProjectTasksById(@NotNull final Long userId, @NotNull final Long projectId) {
        @NotNull val tasksToDelete = findProjectTasksById(userId, projectId);
        tasks.removeAll(tasksToDelete);
    }

    @Override
    public void deleteProjectTaskById(@NotNull final Long userId, @NotNull final Long projectId) {
        for (@NotNull val task : tasks)
            if (userId.equals(task.getUserId()) && projectId.equals(task.getProjectId()))
                task.setProjectId(null);
    }

    private boolean isProjectTaskExist(@NotNull final Long projectId, @NotNull final Task task) {
        @Nullable val currentProjectId = task.getProjectId();

        val isProjectExist = Optional.ofNullable(currentProjectId).isPresent();
        if (isProjectExist) return projectId.equals(currentProjectId);
        return false;
    }

}
