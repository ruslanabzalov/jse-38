package tsc.abzalov.tm.repository;

import org.jetbrains.annotations.NotNull;
import tsc.abzalov.tm.api.repository.IProjectRepository;
import tsc.abzalov.tm.model.Project;

import java.sql.Connection;

public final class ProjectRepository extends AbstractBusinessEntityRepository<Project>
        implements IProjectRepository {

    public ProjectRepository(@NotNull final Connection connection) {
        super(connection);
    }

}
