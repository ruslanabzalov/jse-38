package tsc.abzalov.tm.repository;

import lombok.SneakyThrows;
import lombok.val;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import tsc.abzalov.tm.api.IBusinessEntityRepository;
import tsc.abzalov.tm.enumeration.Status;
import tsc.abzalov.tm.model.AbstractBusinessEntity;
import tsc.abzalov.tm.model.Project;
import tsc.abzalov.tm.model.Task;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import static tsc.abzalov.tm.enumeration.Status.DONE;
import static tsc.abzalov.tm.enumeration.Status.IN_PROGRESS;

// TODO: Подумать над рефакторингом кода.
public abstract class AbstractBusinessEntityRepository<T extends AbstractBusinessEntity> extends AbstractRepository<T>
        implements IBusinessEntityRepository<T> {

    @NotNull
    private final Connection connection;

    public AbstractBusinessEntityRepository(@NotNull final Connection connection) {
        this.connection = connection;
    }

    @Override
    @SneakyThrows
    public long size() {
        @NotNull val query = (this instanceof ProjectRepository)
                ? "SELECT COUNT(*) FROM project;"
                : "SELECT COUNT(*) FROM task;";
        @NotNull val statement = connection.createStatement();
        @NotNull val result = statement.executeQuery(query);

        if (result.next()) return result.getLong(1);
        else return 0;
    }

    @Override
    public boolean isEmpty() {
        return size() == 0;
    }

    @Override
    @SneakyThrows
    public void create(@NotNull final T entity) {
        @NotNull val query = (entity instanceof Project)
                ? "INSERT INTO project (id, name, description, user_id) VALUES (?, ?, ?, ?);"
                : "INSERT INTO task (id, name, description, user_id) VALUES (?, ?, ?, ?);";
        @NotNull val prepareStatement = this.connection.prepareStatement(query);
        createEntity(prepareStatement, entity);
    }

    @Override
    public void addAll(@NotNull final List<T> entities) {
        entities.forEach(this::create);
    }

    @NotNull
    @Override
    @SneakyThrows
    public List<T> findAll() {
        @NotNull val query = (this instanceof ProjectRepository)
                ? "SELECT id, name, description, start_date, end_date, status, user_id FROM project;"
                : "SELECT id, name, description, start_date, end_date, status, user_id, project_id FROM task;";
        @NotNull val statement = this.connection.createStatement();
        @NotNull val result = statement.executeQuery(query);

        @NotNull val entities = new ArrayList<T>();
        while (result.next()) entities.add(getEntity(result));
        return entities;
    }

    @Nullable
    @Override
    @SneakyThrows
    public T findById(@NotNull final Long id) {
        @NotNull val query = (this instanceof ProjectRepository)
                ? "SELECT id, name, description, start_date, end_date, status, user_id FROM project WHERE id = ?;"
                : "SELECT id, name, description, start_date, end_date, status, user_id, project_id FROM task WHERE id = ?;";
        @NotNull val preparedStatement = this.connection.prepareStatement(query);
        preparedStatement.setLong(1, id);
        @NotNull val result = preparedStatement.executeQuery();
        return getEntity(result);
    }

    @Override
    @SneakyThrows
    @SuppressWarnings("SqlWithoutWhere")
    public void clear() {
        @NotNull val query = (this instanceof ProjectRepository)
                ? "DELETE FROM project;"
                : "DELETE FROM task;";
        @NotNull val statement = this.connection.createStatement();
        statement.executeUpdate(query);
    }

    @Override
    @SneakyThrows
    public void removeById(@NotNull final Long id) {
        @NotNull val query = (this instanceof ProjectRepository)
                ? "DELETE FROM project WHERE id = ?;"
                : "DELETE FROM task WHERE id = ?;";
        @NotNull val preparedStatement = this.connection.prepareStatement(query);
        preparedStatement.setLong(1, id);
        preparedStatement.executeUpdate();
    }

    @Override
    @SneakyThrows
    public long size(@NotNull final Long userId) {
        @NotNull val query = (this instanceof ProjectRepository)
                ? "SELECT COUNT(*) FROM project WHERE user_id = ?;"
                : "SELECT COUNT(*) FROM task WHERE user_id = ?;";
        @NotNull val preparedStatement = connection.prepareStatement(query);
        preparedStatement.setLong(1, userId);
        @NotNull val result = preparedStatement.executeQuery();

        if (result.next()) return result.getLong(1);
        else return 0;
    }

    @Override
    @SneakyThrows
    public boolean isEmpty(@NotNull final Long userId) {
        return size(userId) == 0;
    }

    @Override
    @SneakyThrows
    public int indexOf(@NotNull final Long userId, @NotNull final T entity) {
        @NotNull val entities = findAll(userId);
        return entities.indexOf(entity) + 1;
    }

    @Override
    @NotNull
    @SneakyThrows
    public List<T> findAll(@NotNull final Long userId) {
        @NotNull val query = (this instanceof ProjectRepository)
                ? "SELECT id, name, description, start_date, end_date, user_id FROM project WHERE user_id = ?;"
                : "SELECT id, name, description, start_date, end_date, user_id, project_id FROM task WHERE user_id = ?;";
        @NotNull val preparedStatement = this.connection.prepareStatement(query);
        preparedStatement.setLong(1, userId);
        @NotNull val result = preparedStatement.executeQuery();
        @NotNull val projects = new ArrayList<T>();
        while (result.next()) projects.add(getEntity(result));
        return projects;
    }

    @Nullable
    @Override
    @SneakyThrows
    public T findByIndex(@NotNull final Long userId, final int index) {
        @NotNull val entities = findAll(userId);
        return entities.get(index);
    }

    @Nullable
    @Override
    @SneakyThrows
    public T findByName(@NotNull final Long userId, @NotNull final String name) {
        @NotNull val query = (this instanceof ProjectRepository)
                ? "SELECT id, name, description, start_date, end_date, user_id FROM project WHERE user_id = ? AND name = ?;"
                : "SELECT id, name, description, start_date, end_date, user_id, project_id FROM task WHERE user_id = ? AND name = ?;";
        @NotNull val preparedStatement = this.connection.prepareStatement(query);
        preparedStatement.setLong(1, userId);
        preparedStatement.setString(2, name);
        @NotNull val result = preparedStatement.executeQuery();
        return getEntity(result);
    }

    @Override
    @SneakyThrows
    public void editById(@NotNull final Long id,
                        @NotNull final String name, @NotNull final String description) {
        @NotNull val query = (this instanceof ProjectRepository)
                ? "UPDATE project SET name = ?, description = ? WHERE id = ?;"
                : "UPDATE task SET name = ?, description = ? WHERE id = ?;";
        @NotNull val preparedStatement = this.connection.prepareStatement(query);
        preparedStatement.setString(1, name);
        preparedStatement.setString(2, description);
        preparedStatement.setLong(3, id);
        preparedStatement.executeUpdate();
    }

    @Override
    @SneakyThrows
    public void editByName(@NotNull final Long userId, @NotNull final String name,
                           @NotNull final String description) {
        @NotNull val query = (this instanceof ProjectRepository)
                ? "UPDATE project SET description = ? WHERE user_id = ? AND name = ?;"
                : "UPDATE task SET description = ? WHERE user_id = ? AND name = ?;";
        @NotNull val preparedStatement = this.connection.prepareStatement(query);
        preparedStatement.setString(1, description);
        preparedStatement.setLong(2, userId);
        preparedStatement.setString(3, name);
        preparedStatement.executeUpdate();
    }

    @Override
    @SneakyThrows
    public void clear(@NotNull final Long userId) {
        @NotNull val query = (this instanceof ProjectRepository)
                ? "DELETE FROM project WHERE user_id = ?;"
                : "DELETE FROM task WHERE user_id = ?;";
        @NotNull val preparedStatement = this.connection.prepareStatement(query);
        preparedStatement.setLong(1, userId);
        preparedStatement.executeUpdate();
    }

    @Override
    @SneakyThrows
    public void removeByName(@NotNull final Long userId, @NotNull final String name) {
        @NotNull val query = (this instanceof ProjectRepository)
                ? "DELETE FROM project WHERE user_id = ? AND name = ?;"
                : "DELETE FROM task WHERE user_id = ? AND name = ?;";
        @NotNull val preparedStatement = this.connection.prepareStatement(query);
        preparedStatement.setLong(1, userId);
        preparedStatement.setString(2, name);
        preparedStatement.executeUpdate();
    }

    @Override
    @SneakyThrows
    public void startById(@NotNull final Long id) {
        @NotNull val query = (this instanceof ProjectRepository)
                ? "UPDATE project SET start_date = ?, status = ? WHERE id = ?;"
                : "UPDATE task SET start_date = ?, status = ? WHERE id = ?;";
        @NotNull val preparedStatement = this.connection.prepareStatement(query);
        preparedStatement.setTimestamp(1, Timestamp.valueOf(LocalDateTime.now()));
        preparedStatement.setString(2, IN_PROGRESS.toString());
        preparedStatement.setLong(3, id);
        preparedStatement.executeUpdate();
    }

    @Override
    @SneakyThrows
    public void endById(@NotNull final Long id) {
        @NotNull val query = (this instanceof ProjectRepository)
                ? "UPDATE project SET end_date = ?, status = ? WHERE id = ?;"
                : "UPDATE task SET end_date = ?, status = ? WHERE id = ?;";
        @NotNull val preparedStatement = this.connection.prepareStatement(query);
        preparedStatement.setTimestamp(1, Timestamp.valueOf(LocalDateTime.now()));
        preparedStatement.setString(2, DONE.toString());
        preparedStatement.setLong(3, id);
        preparedStatement.executeUpdate();
    }

    @NotNull
    @Override
    @SneakyThrows
    public List<T> sortByName(@NotNull final Long userId) {
        @NotNull val query = (this instanceof ProjectRepository)
                ? "SELECT id, name, description, start_date, end_date, status, user_id FROM project WHERE user_id = ? ORDER BY name;"
                : "SELECT id, name, description, start_date, end_date, status, user_id, project_id FROM task WHERE user_id = ? ORDER BY name;";
        return getSortedEntities(userId, query);
    }

    @NotNull
    @Override
    public List<T> sortByStartDate(@NotNull final Long userId) {
        @NotNull val query = (this instanceof ProjectRepository)
                ? "SELECT id, name, description, start_date, end_date, status, user_id FROM project WHERE user_id = ? ORDER BY start_date;"
                : "SELECT id, name, description, start_date, end_date, status, user_id, project_id FROM task WHERE user_id = ? ORDER BY start_date;";
        return getSortedEntities(userId, query);
    }

    @NotNull
    @Override
    public List<T> sortByEndDate(@NotNull final Long userId) {
        @NotNull val query = (this instanceof ProjectRepository)
                ? "SELECT id, name, description, start_date, end_date, status, user_id FROM project WHERE user_id = ? ORDER BY end_date;"
                : "SELECT id, name, description, start_date, end_date, status, user_id, project_id FROM task WHERE user_id = ? ORDER BY end_date;";
        return getSortedEntities(userId, query);
    }

    @NotNull
    @Override
    public List<T> sortByStatus(@NotNull final Long userId) {
        @NotNull val query = (this instanceof ProjectRepository)
                ? "SELECT id, name, description, start_date, end_date, status, user_id FROM project WHERE user_id = ? ORDER BY status;"
                : "SELECT id, name, description, start_date, end_date, status, user_id, project_id FROM task WHERE user_id = ? ORDER BY status;";
        return getSortedEntities(userId, query);
    }

    @SneakyThrows
    private void createEntity(@NotNull final PreparedStatement statement,
                              @NotNull final AbstractBusinessEntity entity) {
        statement.setLong(1, entity.getId());
        statement.setString(2, entity.getName());
        statement.setString(3, entity.getDescription());
        statement.setLong(4, entity.getUserId());
        statement.executeUpdate();
    }

    @SneakyThrows
    private List<T> getSortedEntities(@NotNull final Long userId, @NotNull final String query) {
        @NotNull val preparedStatement = this.connection.prepareStatement(query);
        preparedStatement.setLong(1, userId);
        @NotNull val result = preparedStatement.executeQuery();

        @NotNull final ArrayList<T> entities = new ArrayList<>();
        while (result.next()) entities.add(getEntity(result));
        return  entities;
    }

    @SneakyThrows
    private T getEntity(@NotNull final ResultSet result) {
        if (!result.next()) return null;
        if (this instanceof ProjectRepository) {
            @NotNull val project = new Project();
            project.setId(result.getLong(1));
            project.setName(result.getString(2));
            project.setDescription(result.getString(3));
            @Nullable val startDate = (result.getTimestamp(4) != null)
                    ? result.getTimestamp(4).toLocalDateTime()
                    : null;
            project.setStartDate(startDate);
            @Nullable val endDate = (result.getTimestamp(5) != null)
                    ? result.getTimestamp(5).toLocalDateTime()
                    : null;
            project.setEndDate(endDate);
            project.setStatus(Status.valueOf(result.getString(6)));
            project.setUserId(result.getLong(7));
            return (T) project;
        }
        else {
            @NotNull val task = new Task();
            task.setId(result.getLong(1));
            task.setName(result.getString(2));
            task.setDescription(result.getString(3));
            @Nullable val startDate = (result.getTimestamp(4) != null)
                    ? result.getTimestamp(4).toLocalDateTime()
                    : null;
            task.setStartDate(startDate);
            @Nullable val endDate = (result.getTimestamp(5) != null)
                    ? result.getTimestamp(5).toLocalDateTime()
                    : null;
            task.setEndDate(endDate);
            task.setStatus(Status.valueOf(result.getString(6)));
            task.setUserId(result.getLong(7));
            task.setProjectId(result.getLong(8));
            return (T) task;
        }
    }

}
