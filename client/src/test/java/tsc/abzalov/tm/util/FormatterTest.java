package tsc.abzalov.tm.util;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertAll;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static tsc.abzalov.tm.util.Formatter.formatBytes;

class FormatterTest {

    @Test
    @DisplayName("Test of Byte Formatter")
    void formatBytesTest() {
        assertAll(() -> {
            assertEquals("0 B", formatBytes(0));
            assertEquals("1 KB", formatBytes(1024));
            assertEquals("1 MB", formatBytes((long) Math.pow(1024, 2)));
            assertEquals("1 GB", formatBytes((long) Math.pow(1024, 3)));
            assertEquals("1 TB", formatBytes((long) Math.pow(1024, 4)));
        });
    }

}