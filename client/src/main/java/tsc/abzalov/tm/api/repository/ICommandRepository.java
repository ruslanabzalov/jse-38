package tsc.abzalov.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import tsc.abzalov.tm.api.service.IServiceLocator;
import tsc.abzalov.tm.command.AbstractCommand;

import java.util.Collection;

public interface ICommandRepository {

    void initCommands(@NotNull IServiceLocator serviceLocator);

    @NotNull
    Collection<AbstractCommand> getCommands();

    @NotNull
    Collection<AbstractCommand> getStartupCommands();

    @NotNull
    Collection<AbstractCommand> getArgumentsCommands();

    @NotNull
    Collection<String> getCommandNames();

    @NotNull
    Collection<String> getCommandArguments();

    @Nullable
    AbstractCommand getCommandByName(@NotNull String name);

    @Nullable
    AbstractCommand getArgumentByName(@NotNull String name);

}
