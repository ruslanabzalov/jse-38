
package tsc.abzalov.tm.endpoint;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the tsc.abzalov.tm.endpoint package. 
 * &lt;p&gt;An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _CloseSession_QNAME = new QName("http://endpoint.tm.abzalov.tsc/", "closeSession");
    private final static QName _CloseSessionResponse_QNAME = new QName("http://endpoint.tm.abzalov.tsc/", "closeSessionResponse");
    private final static QName _FindSession_QNAME = new QName("http://endpoint.tm.abzalov.tsc/", "findSession");
    private final static QName _FindSessionResponse_QNAME = new QName("http://endpoint.tm.abzalov.tsc/", "findSessionResponse");
    private final static QName _OpenSession_QNAME = new QName("http://endpoint.tm.abzalov.tsc/", "openSession");
    private final static QName _OpenSessionResponse_QNAME = new QName("http://endpoint.tm.abzalov.tsc/", "openSessionResponse");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: tsc.abzalov.tm.endpoint
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link CloseSession }
     * 
     */
    public CloseSession createCloseSession() {
        return new CloseSession();
    }

    /**
     * Create an instance of {@link CloseSessionResponse }
     * 
     */
    public CloseSessionResponse createCloseSessionResponse() {
        return new CloseSessionResponse();
    }

    /**
     * Create an instance of {@link FindSession }
     * 
     */
    public FindSession createFindSession() {
        return new FindSession();
    }

    /**
     * Create an instance of {@link FindSessionResponse }
     * 
     */
    public FindSessionResponse createFindSessionResponse() {
        return new FindSessionResponse();
    }

    /**
     * Create an instance of {@link OpenSession }
     * 
     */
    public OpenSession createOpenSession() {
        return new OpenSession();
    }

    /**
     * Create an instance of {@link OpenSessionResponse }
     * 
     */
    public OpenSessionResponse createOpenSessionResponse() {
        return new OpenSessionResponse();
    }

    /**
     * Create an instance of {@link Session }
     * 
     */
    public Session createSession() {
        return new Session();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CloseSession }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link CloseSession }{@code >}
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.abzalov.tsc/", name = "closeSession")
    public JAXBElement<CloseSession> createCloseSession(CloseSession value) {
        return new JAXBElement<CloseSession>(_CloseSession_QNAME, CloseSession.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CloseSessionResponse }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link CloseSessionResponse }{@code >}
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.abzalov.tsc/", name = "closeSessionResponse")
    public JAXBElement<CloseSessionResponse> createCloseSessionResponse(CloseSessionResponse value) {
        return new JAXBElement<CloseSessionResponse>(_CloseSessionResponse_QNAME, CloseSessionResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FindSession }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link FindSession }{@code >}
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.abzalov.tsc/", name = "findSession")
    public JAXBElement<FindSession> createFindSession(FindSession value) {
        return new JAXBElement<FindSession>(_FindSession_QNAME, FindSession.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FindSessionResponse }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link FindSessionResponse }{@code >}
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.abzalov.tsc/", name = "findSessionResponse")
    public JAXBElement<FindSessionResponse> createFindSessionResponse(FindSessionResponse value) {
        return new JAXBElement<FindSessionResponse>(_FindSessionResponse_QNAME, FindSessionResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OpenSession }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link OpenSession }{@code >}
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.abzalov.tsc/", name = "openSession")
    public JAXBElement<OpenSession> createOpenSession(OpenSession value) {
        return new JAXBElement<OpenSession>(_OpenSession_QNAME, OpenSession.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OpenSessionResponse }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link OpenSessionResponse }{@code >}
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.abzalov.tsc/", name = "openSessionResponse")
    public JAXBElement<OpenSessionResponse> createOpenSessionResponse(OpenSessionResponse value) {
        return new JAXBElement<OpenSessionResponse>(_OpenSessionResponse_QNAME, OpenSessionResponse.class, null, value);
    }

}
