package tsc.abzalov.tm.command.sorting;

import lombok.val;
import lombok.var;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import tsc.abzalov.tm.api.service.IServiceLocator;
import tsc.abzalov.tm.command.AbstractCommand;
import tsc.abzalov.tm.enumeration.CommandType;

import static tsc.abzalov.tm.enumeration.CommandType.SORTING_COMMAND;


public final class SortingProjectsByNameCommand extends AbstractCommand {

    public SortingProjectsByNameCommand(@NotNull final IServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @NotNull
    @Override
    public String getCommandName() {
        return "sort-projects-by-name";
    }

    @Nullable
    @Override
    public String getCommandArgument() {
        return null;
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Sort projects by name.";
    }

    @NotNull
    @Override
    public CommandType getCommandType() {
        return SORTING_COMMAND;
    }

    @Override
    public void execute() {
        System.out.println("ALL PROJECTS LIST SORTED BY NAME");
        @NotNull val projectEndpoint = getServiceLocator().getProjectEndpoint();
        @NotNull val session = getServiceLocator().getSession();

        val areProjectsExist = projectEndpoint.projectsSize(session) != 0;
        if (areProjectsExist) {
            @NotNull val projects = projectEndpoint.sortProjectsByName(session);
            var projectIndex = 0;
            for (@NotNull val project : projects) {
                projectIndex = projectEndpoint.projectIndex(session, project) + 1;
                System.out.println(projectIndex + ". " + project);
            }

            System.out.println();
            return;
        }

        System.out.println("Projects list is empty.\n");
    }

}
