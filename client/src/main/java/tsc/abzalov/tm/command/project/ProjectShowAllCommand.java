package tsc.abzalov.tm.command.project;

import lombok.val;
import lombok.var;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import tsc.abzalov.tm.api.service.IServiceLocator;
import tsc.abzalov.tm.command.AbstractCommand;
import tsc.abzalov.tm.enumeration.CommandType;

import static tsc.abzalov.tm.enumeration.CommandType.PROJECT_COMMAND;


public final class ProjectShowAllCommand extends AbstractCommand {

    public ProjectShowAllCommand(@NotNull final IServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @Override
    @NotNull
    public String getCommandName() {
        return "show-all-projects";
    }

    @Override
    @Nullable
    public String getCommandArgument() {
        return null;
    }

    @Override
    @NotNull
    public String getDescription() {
        return "Show all projects.";
    }

    @Override
    @NotNull
    public CommandType getCommandType() {
        return PROJECT_COMMAND;
    }

    @Override
    public void execute() {
        System.out.println("ALL PROJECTS LIST");
        @NotNull val projectEndpoint = getServiceLocator().getProjectEndpoint();
        @NotNull val session = getServiceLocator().getSession();

        val areProjectsExist = projectEndpoint.projectsSize(session) != 0;
        if (areProjectsExist) {
            @NotNull val projects = projectEndpoint.findAllProjects(session);
            var projectIndex = 0;
            for (@NotNull val project : projects) {
                projectIndex = projectEndpoint.projectIndex(session, project) + 1;
                System.out.println(projectIndex + ". " + project);
            }
        }
    }

}
