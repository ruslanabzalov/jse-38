package tsc.abzalov.tm.command.user;

import lombok.SneakyThrows;
import lombok.val;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import tsc.abzalov.tm.api.service.IServiceLocator;
import tsc.abzalov.tm.command.AbstractCommand;
import tsc.abzalov.tm.enumeration.CommandType;
import tsc.abzalov.tm.exception.auth.CannotLockCurrentUserException;
import tsc.abzalov.tm.exception.auth.UserIsNotExistException;

import java.util.Optional;

import static tsc.abzalov.tm.enumeration.CommandType.ADMIN_COMMAND;
import static tsc.abzalov.tm.util.InputUtil.inputId;


public final class UserLockUnlockByIdCommand extends AbstractCommand {

    public UserLockUnlockByIdCommand(@NotNull final IServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @Override
    @NotNull
    public String getCommandName() {
        return "lock-unlock-user-by-id";
    }

    @Override
    @Nullable
    public String getCommandArgument() {
        return null;
    }

    @Override
    @NotNull
    public String getDescription() {
        return "Lock/unlock user by id.";
    }

    @Override
    @NotNull
    public CommandType getCommandType() {
        return ADMIN_COMMAND;
    }

    @Override
    @SneakyThrows
    public void execute() {
        System.out.println("LOCK/UNLOCK USER BY ID");
        @NotNull val userId = inputId();
        @NotNull val session = getServiceLocator().getSession();
        @NotNull val currentUserId = session.getUserId();
        if (userId.equals(currentUserId)) throw new CannotLockCurrentUserException();

        @NotNull val adminEndpoint = getServiceLocator().getAdminEndpoint();
        @Nullable val lockedUnlockedUser = adminEndpoint.adminLockUnlockUserById(session, userId);
        Optional.ofNullable(lockedUnlockedUser).orElseThrow(UserIsNotExistException::new);

        System.out.println("User successfully locked/unlocked by admin.\n");
    }

}
